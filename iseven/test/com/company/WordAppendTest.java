package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WordAppendTest {

    WordAppend wordAppend = new WordAppend();

    @Test
    public void wordAppend() {
        assertEquals("a", wordAppend.wordAppend(new String[]{"a", "b", "a"}));
        assertEquals("aa", wordAppend.wordAppend(new String[]{"a", "b", "a", "c", "a", "d", "a"}));
        assertEquals("thisandthat", wordAppend.wordAppend(new String[]{"this", "or", "that", "and", "this", "and", "that"}));
    }
}