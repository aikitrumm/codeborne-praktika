package com.company;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MergeTwoTest {

    MergeTwo mergeTwo = new MergeTwo();

    @Test
    public void mergeTwo() {
        assertArrayEquals(new String[]{"a", "b"}, mergeTwo.mergeTwo(new String[]{"a", "b"}, new String[]{"a", "b"}, 2));
        assertArrayEquals(new String[]{"a", "b", "c"}, mergeTwo.mergeTwo(new String[]{"a", "c", "e"}, new String[]{"b", "d", "f"}, 3));
        assertArrayEquals(new String[]{"a", "b", "c"}, mergeTwo.mergeTwo(new String[]{"a", "b", "c"}, new String[]{"a", "b", "c"}, 3));
        assertArrayEquals(new String[]{"a", "c", "f"}, mergeTwo.mergeTwo(new String[]{"a", "c", "z"}, new String[]{"c", "f", "z"}, 3));
        assertArrayEquals(new String[]{"a", "b", "c"}, mergeTwo.mergeTwo(new String[]{"a", "c", "z"}, new String[]{"a", "b", "c", "z"}, 3));
    }
}