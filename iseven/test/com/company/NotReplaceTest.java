package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NotReplaceTest {

    NotReplace notReplace = new NotReplace();

    @Test
    public void notReplace_noIs() {
        assertEquals("", notReplace.notReplace(""));
        assertEquals("my", notReplace.notReplace("my"));
    }

    @Test
    public void notReplace_isSidesAreNotLetters() {
        assertEquals("is not", notReplace.notReplace("is"));
        assertEquals("is not test", notReplace.notReplace("is test"));
        assertEquals("is not-is not", notReplace.notReplace("is-is"));
    }

    @Test
    public void notReplace_isSidesAreLetters() {
        assertEquals("isis", notReplace.notReplace("isis"));
        assertEquals("This is not right", notReplace.notReplace("This is right"));
        assertEquals("is not his", notReplace.notReplace("is his"));
    }


}