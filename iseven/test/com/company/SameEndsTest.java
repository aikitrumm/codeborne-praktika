package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SameEndsTest {

    SameEnds sameEnds = new SameEnds();

    @Test
    public void sameEnds() {
        assertEquals("", sameEnds.sameEnds(""));
        assertEquals("", sameEnds.sameEnds("ab"));
        assertEquals("a", sameEnds.sameEnds("aa"));
        assertEquals("a", sameEnds.sameEnds("aaa"));
        assertEquals("ab", sameEnds.sameEnds("abab"));
        assertEquals("java", sameEnds.sameEnds("javaXYZjava"));
    }
}