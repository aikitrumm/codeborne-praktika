package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromeTest {

    @Test
    public void isPalindromeEmptyTest() {
        assertTrue(Palindrome.isPalindrome(""));
    }

    @Test
    public void isPalindromeOneLetterTest() {
        assertTrue(Palindrome.isPalindrome("a"));
    }

    @Test
    public void isPalindromeMoreLettersCaseSensitiveTest() {
        assertTrue(Palindrome.isPalindrome("aa"));
    }

    @Test
    public void isNotPalindromeTwoLettersCaseSensitiveTest() {
        assertFalse(Palindrome.isPalindrome("Aa"));
    }

    @Test
    public void isNotPalindromeMoreLettersCaseSensitiveTest() {
        assertFalse(Palindrome.isPalindrome("ab"));
    }

    @Test
    public void isPalindromeSentenceCaseSensitiveTest() {
        assertTrue(Palindrome.isPalindrome("aias sadas saia"));
    }

    @Test
    public void isNotPalindromeSentenceCaseSensitiveTest() {
        assertFalse(Palindrome.isPalindrome("aias sadas"));
    }

    @Test
    public void isPalindromeEmptyRecursiveCaseSensitiveTest() {
        assertTrue(Palindrome.isPalindromeRecursive(""));
    }

    @Test
    public void isPalindromeOneLetterRecursiveCaseSensitiveTest() {
        assertTrue(Palindrome.isPalindromeRecursive("a"));
    }

    @Test
    public void isPalindromeMoreLettersRecursiveCaseSensitiveTest() {
        assertTrue(Palindrome.isPalindromeRecursive("aa"));
    }

    @Test
    public void isNotPalindromeMoreLettersRecursiveCaseSensitiveTest() {
        assertFalse(Palindrome.isPalindromeRecursive("ab"));
    }

    @Test
    public void isPalindromeSentenceRecursiveCaseSensitiveTest() {
        assertTrue(Palindrome.isPalindromeRecursive("aias sadas saia"));
    }

    @Test
    public void isNotPalindromeSentenceRecursiveCaseSensitiveTest() {
        assertFalse(Palindrome.isPalindromeRecursive("aias sadas"));
    }
}
