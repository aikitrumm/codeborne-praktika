package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserCompareTest {

    UserCompare userCompare = new UserCompare();

    @Test
    public void userCompare() {
        assertEquals(0, userCompare.userCompare("aa", 1, "aa", 1));
        assertEquals(-1, userCompare.userCompare("aa", 1, "bb", 2));
        assertEquals(-1, userCompare.userCompare("aa", 1, "zz", 2));
        assertEquals(1, userCompare.userCompare("hh", 1, "aa", 2));
        assertEquals(1, userCompare.userCompare("bb", 5, "bb", 1));
    }
}