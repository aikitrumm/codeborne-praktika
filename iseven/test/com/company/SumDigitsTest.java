package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumDigitsTest {
    SumDigits sumDigits = new SumDigits();

    @Test
    public void sumDigits() {
        assertEquals(0, sumDigits.sumDigits(""));
        assertEquals(1, sumDigits.sumDigits("1"));
        assertEquals(1, sumDigits.sumDigits("a1a"));
    }

}