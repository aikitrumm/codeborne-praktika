package com.company;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.*;

public class GuessingGameTest {

    private InputStream systemIn = System.in;
    private PrintStream systemOut = System.out;

    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput() {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }

    private void provideInput(String data) {
        ByteArrayInputStream testIn = new ByteArrayInputStream(data.getBytes());
        System.setIn(testIn);
    }

    private String getOutput() {
        return testOut.toString();
    }

    @After
    public void restoreSystemInputOutput() {
        System.setIn(systemIn);
        System.setOut(systemOut);
    }

    @Test
    public void guessingGameUiTest() {
        String input = "15\n" + "90\n" + "r\n" + "q\n";
        String output = "Arva number vahemikus 1-100\n" +
                "[q] lõpetab): Saad uuesti arvata. Number peab olema suurem.\n" +
                "Paku uus number: Saad uuesti arvata. Number peab olema väiksem.\n" +
                "Paku uus number: Sisesta number või lõpeta [q]: \n";
        provideInput(input);
        Scanner reader = new Scanner(input);
        GuessingGame.ui(reader, 80);
        assertEquals(output, getOutput());
    }


    @Test
    public void isANumberTest() {

        assertTrue(GuessingGame.isANumber("88"));
    }

    @Test
    public void notANumberTest() {

        assertFalse(GuessingGame.isANumber("w"));
    }

    @Test
    public void isSmallerNumberTest() {
        assertTrue(GuessingGame.isNumberSmaller(58, "6"));
    }

    @Test
    public void isBiggerNumberTest() {
        assertTrue(GuessingGame.isNumberBigger(58, "98"));
    }

    @Test
    public void isEqualNumberTest() {
        assertTrue(GuessingGame.isNumberEqual(58, "58"));
    }

    @Test
    public void isQTest() {
        assertTrue(GuessingGame.isQ("q"));
    }

    @Test
    public void isOtherSymbolTest() {
        assertTrue(GuessingGame.notQOrNumber("#"));
    }

}