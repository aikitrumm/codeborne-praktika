package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScoreUpTest {

    ScoreUp scoreUp = new ScoreUp();

    @Test
    public void scoreUp() {
        assertEquals(4, scoreUp.scoreUp(new String[]{"a"}, new String[]{"a"}));
        assertEquals(-1, scoreUp.scoreUp(new String[]{"a"}, new String[]{"b"}));
        assertEquals(0, scoreUp.scoreUp(new String[]{"a"}, new String[]{"?"}));
        assertEquals(3, scoreUp.scoreUp(new String[]{"a", "b", "c"}, new String[]{"a", "a", "?"}));

    }
}