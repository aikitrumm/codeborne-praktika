package com.company;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HasOneTest {

    HasOne hasOne = new HasOne();

    @Test
    public void hasOne() {
        assertFalse(hasOne.hasOne(0));
        assertTrue(hasOne.hasOne(1));
        assertTrue(hasOne.hasOne(10));
        assertTrue(hasOne.hasOne(21));
        assertTrue(hasOne.hasOne(212));
        assertTrue(hasOne.hasOne(5155));
        assertTrue(hasOne.hasOne(56156));
    }

}