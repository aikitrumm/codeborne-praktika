package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WithoutStringTest {

    WithoutString withoutString = new WithoutString();

    @Test
    public void withoutString() {
        assertEquals("", withoutString.withoutString("a", "a"));
        assertEquals("b", withoutString.withoutString("ab", "a"));
        assertEquals("Hllo thr", withoutString.withoutString("Hello there", "e"));
        assertEquals("TH  a FH", withoutString.withoutString("THIS is a FISH", "iS"));

    }

}