package com.company;

public class ScoresSpecial {
    public int scoresSpecial(int[] a, int[] b) {
        return getLargestSpecialScore(a) + getLargestSpecialScore(b);
    }

    public int getLargestSpecialScore(int[] c) {
        int largest = 0;
        if (c.length > 0 && c[0] % 10 == 0) {
            largest = c[0];
        }

        for (int i = 0; i < c.length - 1; i++) {
            if (c.length > 0 && largest < c[i + 1] && c[i + 1] % 10 == 0) {
                largest = c[i + 1];
            }
        }
        return largest;
    }
}
