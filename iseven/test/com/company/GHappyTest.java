package com.company;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GHappyTest {

    GHappy gHappyInTest = new GHappy();

    @Test
    public void gHappy() {
        assertTrue(gHappyInTest.gHappy(""));
        assertFalse(gHappyInTest.gHappy("g"));
        assertTrue(gHappyInTest.gHappy("gg"));
        assertFalse(gHappyInTest.gHappy("ag"));
        assertFalse(gHappyInTest.gHappy("ga"));
        assertFalse(gHappyInTest.gHappy("aa"));
        assertTrue(gHappyInTest.gHappy("ggg"));
        assertTrue(gHappyInTest.gHappy("gga"));
        assertTrue(gHappyInTest.gHappy("agg"));
        assertFalse(gHappyInTest.gHappy("gag"));
        assertFalse(gHappyInTest.gHappy("aag"));
        assertFalse(gHappyInTest.gHappy("gaa"));
        assertFalse(gHappyInTest.gHappy("aaa"));
        assertTrue(gHappyInTest.gHappy("gggg"));
        assertTrue(gHappyInTest.gHappy("ggga"));
        assertTrue(gHappyInTest.gHappy("ggaa"));
        assertTrue(gHappyInTest.gHappy("gagg"));
        assertFalse(gHappyInTest.gHappy("ggag"));
        assertFalse(gHappyInTest.gHappy("gaag"));
        assertFalse(gHappyInTest.gHappy("gaga"));
        assertFalse(gHappyInTest.gHappy("agag"));
        assertTrue(gHappyInTest.gHappy("xxgggxy"));
        assertFalse(gHappyInTest.gHappy("xxggxyg"));
    }
}