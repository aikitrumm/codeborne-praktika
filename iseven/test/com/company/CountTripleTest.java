package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountTripleTest {

    CountTriple countTriple = new CountTriple();

    @Test
    public void countTriple() {
        assertEquals(0, countTriple.countTriple(""));
        assertEquals(0, countTriple.countTriple("x"));
        assertEquals(0, countTriple.countTriple("xx"));
        assertEquals(1, countTriple.countTriple("xxx"));
        assertEquals(0, countTriple.countTriple("xxa"));
        assertEquals(2, countTriple.countTriple("xxxx"));
        assertEquals(0, countTriple.countTriple("xxax"));
        assertEquals(1, countTriple.countTriple("axxx"));
        assertEquals(3, countTriple.countTriple("xxxabyyyycd"));
    }

}