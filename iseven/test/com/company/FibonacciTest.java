package com.company;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.company.Fibonacci.*;
import static java.util.Arrays.*;
import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class FibonacciTest {
    @Test
    public void fibonacciRecursive1NumberInSequenceTest() {
        assertEquals(Collections.singletonList(0), fibonacciRecursive(1));
    }

    @Test
    public void fibonacciRecursive2NumbersInSequenceTest() {
        assertEquals(asList(0, 1), fibonacciRecursive(2));
    }

    @Test
    public void fibonacciRecursive3NumbersInSequenceTest() {
        assertEquals(asList(0, 1, 1), fibonacciRecursive(3));
    }

    @Test
    public void fibonacciRecursiveManyNumbersInSequenceTest() {
        assertEquals(asList(0, 1, 1, 2, 3), fibonacciRecursive(5));
    }

    @Test
    public void fibonacciForLoop1NumberInSequenceTest() {
        assertEquals(Collections.singletonList(0), fibonacciForLoop(1));
    }

    @Test
    public void fibonacciForLoop2NumbersInSequenceTest() {
        assertEquals(asList(0, 1), fibonacciForLoop(2));
    }

    @Test
    public void fibonacciForLoopManyNumbersInSequenceTest() {
        assertEquals(asList(0, 1, 1, 2, 3, 5, 8), fibonacciForLoop(7));
    }

    @Test
    public void fibonacciWhileLoop1NumberInSequenceTest() {
        assertEquals(Collections.singletonList(0), fibonacciWhileLoop(1));
    }

    @Test
    public void fibonacciWhileLoop2NumberSInSequenceTest() {
        assertEquals(asList(0, 1), fibonacciWhileLoop(2));
    }

    @Test
    public void fibonacciWhileLoopManyNumbersInSequenceTest() {
        assertEquals(asList(0, 1, 1, 2, 3, 5, 8), fibonacciWhileLoop(7));
    }

    @Test
    public void fibonacciTailRecursive1NumberInSequenceTest() {
        List<Integer> a = new ArrayList<>(Collections.singletonList(0));
        List<Integer> b = new ArrayList<>(asList(0, 1));

        assertEquals(Collections.singletonList(0), fibonacciTailRecursionList(1, a, b));
    }

    @Test
    public void fibonacciTailRecursive2NumbersInSequenceTest() {
        List<Integer> a = new ArrayList<>(Collections.singletonList(0));
        List<Integer> b = new ArrayList<>(asList(0, 1));
        assertEquals(asList(0, 1), fibonacciTailRecursionList(2, a, b));
    }

    @Test
    public void fibonacciTailRecursive3NumbersInSequenceTest() {
        List<Integer> a = new ArrayList<>(Collections.singletonList(0));
        List<Integer> b = new ArrayList<>(asList(0, 1));
        assertEquals(asList(0, 1, 1), fibonacciTailRecursionList(3, a, b));
    }

    @Test
    public void fibonacciTailRecursiveManyNumbersInSequenceTest() {
        List<Integer> a = new ArrayList<>(Collections.singletonList(0));
        List<Integer> b = new ArrayList<>(asList(0, 1));
        List<Integer> result = new ArrayList<>(asList(0, 1, 1, 2, 3, 5, 8, 13, 21, 34));
        assertEquals(result, fibonacciTailRecursionList(10, a, b));
    }
}