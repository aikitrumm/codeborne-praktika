package com.company;

import org.junit.Test;

import java.util.Arrays;

import static com.company.Stream.doubleInput;
import static com.company.Stream.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class StreamTest {

    @Test
    public void doubleInputTest() {
        int[] array = {2, 4};
        int[] arrayDouble = {4, 8};
        assertArrayEquals(arrayDouble, doubleInput(array));
    }

    @Test
    public void returnEvenAndDoubleTest() {
        int[] array = {1, 2, 4, 9};
        int[] arrayEvenDouble = {4, 8};

        assertArrayEquals(arrayEvenDouble, evenAndDoubleInput(array));
    }

    @Test
    public void returnPrimeAndDoubleTest() {
        int[] array = {9, 181, 5, 4, 1, 11};
        int[] arrayPrimeDoubleSorted = {10, 22, 362};

        assertEquals(Arrays.toString(arrayPrimeDoubleSorted), Arrays.toString(returnPrimeAndDoubleAndSorted(array)));
    }
}