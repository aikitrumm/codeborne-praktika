package com.company;

import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class WebServerFileTest {
    private WebServerFile server = new WebServerFile();

    @Test
    public void getClientInputTest() {
        String inputStreamText = "" +
                "GET /helloworld.jpg HTTP/1.1\n" +
                "Host: localhost:8889\n" +
                "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0\n" +
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\n" +
                "Accept-Language: en-US,en;q=0.5\n\n";

        InputStream targetStream = new ByteArrayInputStream(inputStreamText.getBytes());
        BufferedReader in = new BufferedReader(new InputStreamReader(targetStream));
        assertEquals("", server.getClientInput(in));
    }

    @Test
    public void getNameFromQueryTest() {
        String queryAiki = "name=Aiki";
        String expectedOutputAiki = "Aiki";
        String queryNull = null;
        String expectedOutputNull = "";
        assertEquals(expectedOutputAiki, server.ifExistsGetNameFromQuery(queryAiki));
        assertEquals(expectedOutputNull, server.ifExistsGetNameFromQuery(queryNull));
    }

    @Test
    public void getFileExtensionTest() {
        assertEquals("ico", server.getFileExtension("favicon.ico"));
        assertEquals("txt", server.getFileExtension("file.txt"));
        assertEquals("html", server.getFileExtension("helloworld.html"));
        assertEquals("jpg", server.getFileExtension("helloworld.jpg"));
        assertEquals("hello", server.getFileExtension("hello"));
    }
    @Test
    public void sendResponseToClientTest() {
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        byte[] informationToSend = {1, 2, 3};
        server.sendResponseToClient(outContent, informationToSend);
        assertArrayEquals(informationToSend, outContent.toByteArray());
    }

    @Test
    public void makeHeadersTest() {
        String expectedOutput = "" +
                "HTTP/1.0 200 OK\n" +
                "Content-Type: text/html; charset=utf-8\n" +
                "Content-Length: 3\n" +
                "\n";
        byte[] bytes = {1, 2, 3};
        String statusCode = "200 OK";
        String fileExtension = "html";
        assertEquals(expectedOutput, server.makeHeaders(bytes, statusCode, fileExtension));
    }

    @Test
    public void makeResponseTest() {
        String headers = "headers";
        byte[] fileBytes = {1, 2, 3};
        byte[] expectedOutput = {104, 101, 97, 100, 101, 114, 115, 1, 2, 3};
        assertArrayEquals(expectedOutput, server.makeResponse(headers, fileBytes));
    }

    @Test
    public void getBodyForHelloAndGoodbyeAikiTest() throws UnsupportedEncodingException {
        String expectedResponse = "" +
                "Hello aiki";
        String name = "";
        assertArrayEquals(expectedResponse.getBytes(StandardCharsets.UTF_8), server.getBodyForHelloAndGoodbye("aiki", name));
    }

    @Test
    public void getBodyForHelloAndGoodbyeHelloTest() throws UnsupportedEncodingException {
        String expectedResponse = "" +
                "Hello";
        String name = "";
        assertArrayEquals(expectedResponse.getBytes(StandardCharsets.UTF_8), server.getBodyForHelloAndGoodbye("hello", name));

    }

    @Test
    public void getBodyForHelloAndGoodbyeGoodbyeTest() throws UnsupportedEncodingException {
        String expectedResponse = "" +
                "Goodbye";
        String name = "";
        assertArrayEquals(expectedResponse.getBytes(StandardCharsets.UTF_8), server.getBodyForHelloAndGoodbye("goodbye", name));
    }

    @Test
    public void getBodyForHelloAndGoodbyeEmptyTest() throws UnsupportedEncodingException {
        String expectedResponse = "Hello World!";
        String name = "";
        assertArrayEquals(expectedResponse.getBytes(StandardCharsets.UTF_8), server.getBodyForHelloAndGoodbye("", name));
    }
}