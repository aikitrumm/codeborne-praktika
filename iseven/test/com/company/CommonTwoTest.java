package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommonTwoTest {

    CommonTwo commonTwo = new CommonTwo();

    @Test
    public void commonTwo() {
        assertEquals(1, commonTwo.commonTwo(new String[]{"a"}, new String[]{"a"}));
        assertEquals(0, commonTwo.commonTwo(new String[]{"a"}, new String[]{"b"}));
        assertEquals(1, commonTwo.commonTwo(new String[]{"a"}, new String[]{"a", "b"}));
        assertEquals(2, commonTwo.commonTwo(new String[]{"a", "b", "c"}, new String[]{"b", "c"}));
        assertEquals(2, commonTwo.commonTwo(new String[]{"b", "c"}, new String[]{"a", "b", "c"}));
        assertEquals(3, commonTwo.commonTwo(new String[]{"a", "a", "b", "b", "c"}, new String[]{"a", "b", "c"}));
        assertEquals(3, commonTwo.commonTwo(new String[]{"a", "a", "b", "b", "c"}, new String[]{"a", "b", "b", "b", "c"}));
    }
}