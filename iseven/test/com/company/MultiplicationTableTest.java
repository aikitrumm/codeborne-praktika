package com.company;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static com.company.MultiplicationTable.*;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class MultiplicationTableTest {

    @Test
    public void oneByOneTableTest() {
        String lines = "" +
                " * | 1\n" +
                " 1 | 1\n";
        assertEquals(lines, multiplicationTableFixedCellWidth(1, 1));
    }

    @Test
    public void fourByFourTableTest() {
        String lines = "" +
                "  * |  1  2  3  4\n" +
                "  1 |  1  2  3  4\n" +
                "  2 |  2  4  6  8\n" +
                "  3 |  3  6  9 12\n" +
                "  4 |  4  8 12 16\n";
        assertEquals(lines, multiplicationTableFixedCellWidth(4, 4));
    }

    @Test
    public void tenByTenTableTest() {
        String lines = "" +
                "   * |   1   2   3   4   5   6   7   8   9  10\n" +
                "   1 |   1   2   3   4   5   6   7   8   9  10\n" +
                "   2 |   2   4   6   8  10  12  14  16  18  20\n" +
                "   3 |   3   6   9  12  15  18  21  24  27  30\n" +
                "   4 |   4   8  12  16  20  24  28  32  36  40\n" +
                "   5 |   5  10  15  20  25  30  35  40  45  50\n" +
                "   6 |   6  12  18  24  30  36  42  48  54  60\n" +
                "   7 |   7  14  21  28  35  42  49  56  63  70\n" +
                "   8 |   8  16  24  32  40  48  56  64  72  80\n" +
                "   9 |   9  18  27  36  45  54  63  72  81  90\n" +
                "  10 |  10  20  30  40  50  60  70  80  90 100\n";
        assertEquals(lines, multiplicationTableFixedCellWidth(10, 10));
    }

    @Test
    public void multiplicationTableTest() {
        ArrayList<ArrayList<Integer>> multiplicationTable = new ArrayList<>();
        ArrayList<Integer> firstLine = new ArrayList<>(asList(1, 2, 3));
        ArrayList<Integer> secondLine = new ArrayList<>(asList(2, 4, 6));
        ArrayList<Integer> thirdLine = new ArrayList<>(asList(3, 6, 9));
        multiplicationTable.add(firstLine);
        multiplicationTable.add(secondLine);
        multiplicationTable.add(thirdLine);
        assertEquals(multiplicationTable, multiplicationTable(3, 3));
    }

    @Test
    public void cellWidthTwoByTwoTest() {
        assertEquals(asList(2, 2, 2), multiplicationTableCellWidth(3, 3));
    }

    @Test
    public void cellWidthFourByFourTest() {
        assertEquals(asList(2, 2, 3, 3), multiplicationTableCellWidth(4, 4));
    }

    @Test
    public void firstTwoLinesTest() {
        String lines = "" +
                " * | 1 2\n" +
                "--------\n";
        assertEquals(lines, firstTwoLines(2, 2).toString());
    }

    @Test
    public void oneByOneTableWithMultiplicationTableRenderingTest() {
        String lines = "" +
                " * | 1\n" +
                "------\n" +
                " 1 | 1\n";
        assertEquals(lines, multiplicationTableRendering(1, 1));
    }

    @Test
    public void oneLineWithOfMultiplicationTableWithoutStartTest() {
        String lines = "" +
                " 3 6 9";
        assertEquals(lines, createMultiplicationTableNextInLine(3, 3, Arrays.asList(3, 6, 9)).toString());
    }

    @Test
    public void fourByFourWithMultiplicationTableRenderingTest() {
        String lines = "" +
                " * | 1 2  3  4\n" +
                "--------------\n" +
                " 1 | 1 2  3  4\n" +
                " 2 | 2 4  6  8\n" +
                " 3 | 3 6  9 12\n" +
                " 4 | 4 8 12 16\n";
        assertEquals(lines, multiplicationTableRendering(4, 4));
    }

    @Test
    public void fourByFourMultiplicationTableWithoutFirstTwoLinesTest() {
        String lines = "" +
                " 1 | 1 2  3  4\n" +
                " 2 | 2 4  6  8\n" +
                " 3 | 3 6  9 12\n" +
                " 4 | 4 8 12 16\n";
        assertEquals(lines, createMultiplicationTable(4, 4).toString());
    }

    @Test
    public void tenByTenTableWithmultiplicationTableRenderingTest() {
        String lines = "" +
                "  * |  1  2  3  4  5  6  7  8  9  10\n" +
                "------------------------------------\n" +
                "  1 |  1  2  3  4  5  6  7  8  9  10\n" +
                "  2 |  2  4  6  8 10 12 14 16 18  20\n" +
                "  3 |  3  6  9 12 15 18 21 24 27  30\n" +
                "  4 |  4  8 12 16 20 24 28 32 36  40\n" +
                "  5 |  5 10 15 20 25 30 35 40 45  50\n" +
                "  6 |  6 12 18 24 30 36 42 48 54  60\n" +
                "  7 |  7 14 21 28 35 42 49 56 63  70\n" +
                "  8 |  8 16 24 32 40 48 56 64 72  80\n" +
                "  9 |  9 18 27 36 45 54 63 72 81  90\n" +
                " 10 | 10 20 30 40 50 60 70 80 90 100\n";
        assertEquals(lines, multiplicationTableRendering(10, 10));
    }
}