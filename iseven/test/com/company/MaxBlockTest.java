package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaxBlockTest {

    MaxBlock maxBlock = new MaxBlock();

    @Test
    public void maxBlock() {
        assertEquals(0, maxBlock.maxBlock(""));
        assertEquals(1, maxBlock.maxBlock("a"));
        assertEquals(2, maxBlock.maxBlock("aa"));
        assertEquals(2, maxBlock.maxBlock("aab"));
        assertEquals(3, maxBlock.maxBlock("aabbb"));
        assertEquals(3, maxBlock.maxBlock("aaabbb"));
        assertEquals(4, maxBlock.maxBlock("aaaxbbbyyyy"));
        assertEquals(5, maxBlock.maxBlock("22222aaaxbbbyyyy"));
        assertEquals(4, maxBlock.maxBlock("abbCCCddBBBBxx"));
    }

}