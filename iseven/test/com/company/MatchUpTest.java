package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MatchUpTest {

    MatchUp matchUp = new MatchUp();

    @Test
    public void matchUp() {
        assertEquals(1, matchUp.matchUp(new String[]{"x"}, new String[]{"x"}));
        assertEquals(1, matchUp.matchUp(new String[]{"x", "ab"}, new String[]{"y", "ab"}));
        assertEquals(2, matchUp.matchUp(new String[]{"x", "ab", "qwe"}, new String[]{"y", "ab", "qwc"}));
    }
}