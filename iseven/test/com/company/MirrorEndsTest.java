package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MirrorEndsTest {

    MirrorEnds mirrorEnds = new MirrorEnds();

    @Test
    public void mirrorEnds() {
        assertEquals("", mirrorEnds.mirrorEnds(""));
        assertEquals("aa", mirrorEnds.mirrorEnds("aa"));
        assertEquals("aba", mirrorEnds.mirrorEnds("aba"));
        assertEquals("aax", mirrorEnds.mirrorEnds("aaxbcxaa"));
    }
}