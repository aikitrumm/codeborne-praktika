package com.company;



import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;


class ConcatTest {
    private Concat concat = new Concat();


    @Test
    void concatTest() {
        List<String> stringList1 = new ArrayList<>(Arrays.asList("Hello", "World"));
        List<String> stringList2 = new ArrayList<>(Arrays.asList("Hello", "World"));
        List<String> expectedList = asList("Hello", "World", "Hello", "World");

        assertEquals(expectedList, concat.concat(stringList1, stringList2));
    }

    @Test
    void concatTestNotEqual() {
        List<String> stringList1 = new ArrayList<>();
        stringList1.add("Hello");
        stringList1.add("World");

        List<String> stringList2 = new ArrayList<>();
        stringList2.add("Hello");

        List<String> expectedList = new ArrayList<>();
        expectedList.add("Hello");
        expectedList.add("World");
        expectedList.add("Hello");

        assertEquals(expectedList, concat.concat(stringList1, stringList2));
    }

    @Test
    void concatTestFirstEmpty() {
        List<String> stringList1 = new ArrayList<>();

        List<String> stringList2 = new ArrayList<>();
        stringList2.add("Hello");

        List<String> expectedList = new ArrayList<>();
        expectedList.add("Hello");

        assertEquals(expectedList, concat.concat(stringList1, stringList2));
    }

    @Test
    void concatForTest() {
        List<String> stringList1 = new ArrayList<>();
        stringList1.add("Hello");
        stringList1.add("World");

        List<String> stringList2 = new ArrayList<>();
        stringList2.add("Hello");
        stringList2.add("World");

        List<String> expectedList = new ArrayList<>();
        expectedList.add("Hello");
        expectedList.add("World");
        expectedList.add("Hello");
        expectedList.add("World");

        assertEquals(expectedList, concat.concatFor(stringList1, stringList2));
    }

    @Test
    void concatForTestNotEqual() {
        List<String> stringList1 = new ArrayList<>();
        stringList1.add("Hello");
        stringList1.add("World");

        List<String> stringList2 = new ArrayList<>();
        stringList2.add("Hello");

        List<String> expectedList = new ArrayList<>();
        expectedList.add("Hello");
        expectedList.add("World");
        expectedList.add("Hello");

        assertEquals(expectedList, concat.concatFor(stringList1, stringList2));
    }

    @Test
    void concatForTestFirstEmpty() {
        List<String> stringList1 = new ArrayList<>();

        List<String> stringList2 = new ArrayList<>();
        stringList2.add("Hello");

        List<String> expectedList = new ArrayList<>();
        expectedList.add("Hello");

        assertEquals(expectedList, concat.concatFor(stringList1, stringList2));
    }
}