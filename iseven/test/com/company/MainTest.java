package com.company;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

class MainTest {


    @Test
    void isEvenTest() {

        assertTrue(Main.isEven(2));
    }

    @Test
    void isOddTest() {

        assertFalse(Main.isEven(1));
    }

    @Test
    void isOddTestNr0() {

        assertTrue(Main.isEven(0));
    }

    @Test
    void isEvenTestNrMinus2() {

        assertTrue(Main.isEven(-2));
    }
    @Test
    void isEvenDifferentTest() {

        assertTrue(Main.isEvenDifferent(2));
    }
    @Test
    void isOddDifferentTest() {

        assertFalse(Main.isEvenDifferent(1));
    }

    @Test
    void isEvenTestDifferentNrMinus2() {

        assertTrue(Main.isEvenDifferent(-2));
    }

    @Test
    void isEvenTestDifferentNrMinus4() {

        assertTrue(Main.isEvenDifferent(-4));
    }
    @Test
    void isEvenTestFlagVariable() {
        assertTrue(Main.isEvenFlagVariable(2));
    }

    @Test
    void isOddTestFlagVariable() {
        assertFalse(Main.isEvenFlagVariable(1));
    }

    @Test
    void isOddTestNr0FlagVariable() {

        assertTrue(Main.isEvenFlagVariable(0));
    }

    @Test
    void isEvenTestNrMinus2FlagVariable() {

        assertTrue(Main.isEvenFlagVariable(-2));
    }
    @Test
    void isEvenTestMod() {

        assertTrue(Main.isEvenWithMod(2));
    }

    @Test
    void isOddTestMod() {

        assertFalse(Main.isEvenWithMod(1));
    }

    @Test
    void isOddTestNr0Mod() {

        assertTrue(Main.isEvenWithMod(0));
    }

    @Test
    void isEvenTestNrMinus2Mod() {

        assertTrue(Main.isEvenWithMod(-2));
    }

    @Test
    void isEvenTestString() {

        assertTrue(Main.isEvenString(4));
    }

    @Test
    void isOddTestString() {

        assertFalse(Main.isEvenString(1));
    }

    @Test
    void isOddTestNr0String() {

        assertTrue(Main.isEvenString(0));
    }

    @Test
    void isEvenTestNrMinus2String() {

        assertTrue(Main.isEvenString(-2));
    }


}