package com.company;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class StreamFilterTest {

    private StreamFilter streamFilter = new StreamFilter();

    @Test
    public void filterEvenNumbers() {
        assertEquals(Arrays.asList(2,4,6), StreamFilter.filterEvenNumbers(Arrays.asList(1,2,3,4,5,6)));
    }

    @Test
    public void filterENumbers() {
        assertEquals(Arrays.asList(2, 4, 6), streamFilter.filterEvenNumbersInterface(Arrays.asList(1, 2, 3, 4, 5, 6)));
    }

    @Test
    public void filterOddNumbers() {
        assertEquals(Arrays.asList(1, 3, 5), StreamFilter.filterOddNumbers(Arrays.asList(1, 2, 3, 4, 5, 6)));
    }

    @Test
    public void filterONumbers() {
        assertEquals(Arrays.asList(1, 3, 5), streamFilter.filterOddNumbersInterface(Arrays.asList(1, 2, 3, 4, 5, 6)));
    }

}