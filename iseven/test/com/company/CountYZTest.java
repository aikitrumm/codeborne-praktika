package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class CountYZTest {
    CountYZ countYZ = new CountYZ();

    @Test
    public void countYZ_oneWord() {
        assertEquals(1, countYZ.countYZ("say"));
        assertEquals(1, countYZ.countYZ("saz"));
        assertEquals(0, countYZ.countYZ("sa"));
        assertEquals(1, countYZ.countYZ("saY!"));

    }

    @Test
    public void countYZ_multipleWords() {
        assertEquals(2, countYZ.countYZ("saY say"));
        assertEquals(1, countYZ.countYZ("saz saa"));
        assertEquals(0, countYZ.countYZ("sa sa"));
        assertEquals(3, countYZ.countYZ("say!saz!saZ"));

    }

    @Test
    public void isLastLetterInStringYOrZ() {
        assertTrue(countYZ.isLastCharacterInStringYOrZ("say"));
        assertTrue(countYZ.isLastCharacterInStringYOrZ("saz"));
        assertFalse(countYZ.isLastCharacterInStringYOrZ("sa!"));
        assertFalse(countYZ.isLastCharacterInStringYOrZ("yayza"));
    }

    @Test
    public void isLastLetterInWordYOrZAndFollowedByNonLetter() {
        assertEquals(1, countYZ.countOfYOrZAsLastCharactersInWord("say!"));
        assertEquals(1, countYZ.countOfYOrZAsLastCharactersInWord("saZ!"));
        assertEquals(0, countYZ.countOfYOrZAsLastCharactersInWord("saa!"));
        assertEquals(0, countYZ.countOfYOrZAsLastCharactersInWord("saa!6!"));
        assertEquals(1, countYZ.countOfYOrZAsLastCharactersInWord("saaz!6!"));
        assertEquals(0, countYZ.countOfYOrZAsLastCharactersInWord("!6!"));
        assertEquals(0, countYZ.countOfYOrZAsLastCharactersInWord("saa"));
    }
}