package com.company;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class CopyEvensTest {

    CopyEvens copyEvens = new CopyEvens();

    @Test
    public void copyEvens() {
        assertArrayEquals(new int[]{2}, copyEvens.copyEvens(new int[]{2}, 1));
        assertArrayEquals(new int[]{4}, copyEvens.copyEvens(new int[]{1, 4}, 1));
        assertArrayEquals(new int[]{2, 4}, copyEvens.copyEvens(new int[]{2, 4}, 2));
        assertArrayEquals(new int[]{2, 4}, copyEvens.copyEvens(new int[]{2, 4, 8}, 2));
    }
}