package com.company;

public class UserCompare {
    public int userCompare(String aName, int aId, String bName, int bId) {
        int compare = 0;
        if (aName.compareTo(bName) < 0) compare = -1;
        if (aName.compareTo(bName) == 0) {
            if (aId>bId) {
                compare = 1;
            } else if (aId==bId) {
                compare = 0;
            } else {
                compare = -1;
            }
        }
        if (aName.compareTo(bName) > 0) compare = 1;

        return compare;
    }
}
