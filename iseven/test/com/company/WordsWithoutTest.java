package com.company;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class WordsWithoutTest {

    WordsWithout wordsWithout = new WordsWithout();

    @Test
    public void wordsWithout() {
        assertArrayEquals(new String[]{"b"}, wordsWithout.wordsWithout(new String[]{"b"}, "a"));
        assertArrayEquals(new String[]{"b"}, wordsWithout.wordsWithout(new String[]{"a", "b"}, "a"));
    }
}