package com.company;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DividesSelfTest {
    DividesSelf dividesSelf = new DividesSelf();

    @Test
    public void dividesSelf() {
        assertTrue(dividesSelf.dividesSelf(1));
        assertFalse(dividesSelf.dividesSelf(21));
        assertTrue(dividesSelf.dividesSelf(12));
        assertTrue(dividesSelf.dividesSelf(128));
        assertFalse(dividesSelf.dividesSelf(120));
    }
}