package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScoresSpecialTest {

    ScoresSpecial scoresSpecial = new ScoresSpecial();

    @Test
    public void scoresSpecial() {
        assertEquals(20, scoresSpecial.scoresSpecial(new int[]{10}, new int[]{10}));
        assertEquals(20, scoresSpecial.scoresSpecial(new int[]{20}, new int[]{1}));
        assertEquals(50, scoresSpecial.scoresSpecial(new int[]{20}, new int[]{10, 5, 30}));
        assertEquals(40, scoresSpecial.scoresSpecial(new int[]{12, 10, 4}, new int[]{2, 20, 30}));

    }


    @Test
    public void getLargestSpecialScore() {
        assertEquals(0, scoresSpecial.getLargestSpecialScore(new int[]{9}));
        assertEquals(0, scoresSpecial.getLargestSpecialScore(new int[0]));
        assertEquals(0, scoresSpecial.getLargestSpecialScore(new int[]{9, 7}));
        assertEquals(10, scoresSpecial.getLargestSpecialScore(new int[]{9, 7, 10}));
        assertEquals(30, scoresSpecial.getLargestSpecialScore(new int[]{9, 7, 10, 21, 30}));
    }
}