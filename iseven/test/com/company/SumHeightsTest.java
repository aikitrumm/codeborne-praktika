package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumHeightsTest {

    SumHeights sumHeights = new SumHeights();

    @Test
    public void sumHeights() {
        assertEquals(1, sumHeights.sumHeights(new int[]{1, 2}, 0, 1));
        assertEquals(1, sumHeights.sumHeights(new int[]{4, 3}, 0, 1));
        assertEquals(4, sumHeights.sumHeights(new int[]{4, 0}, 0, 1));
        assertEquals(4, sumHeights.sumHeights(new int[]{0, 4}, 0, 1));
        assertEquals(3, sumHeights.sumHeights(new int[]{0, 4, 1}, 1, 2));
        assertEquals(6, sumHeights.sumHeights(new int[]{1, 4, 1}, 0, 2));

    }
}