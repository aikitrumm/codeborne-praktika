package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PigLatinTest {

    @Test
    public void oneWordTest() {
        assertEquals("ikiaay", PigLatin.pigLatin("aiki"));
    }

    @Test
    public void emptySentenceTest() {
        assertEquals("", PigLatin.pigLatin(""));
    }

    @Test
    public void twoWordsTest() {
        assertEquals("ikiaay rummtay", PigLatin.pigLatin("aiki trumm"));
    }

    @Test
    public void manyWordsDifferentSymbolsTest() {
        assertEquals("ikiaay rummtay !ay ][ay", PigLatin.pigLatin("aiki trumm ! []"));
    }

    @Test
    public void oneWordStreamTest() {
        assertEquals("ikiaay", PigLatin.pigLatinStream("aiki"));
    }

    @Test
    public void oneSymbolStreamTest() {
        assertEquals("#ay", PigLatin.pigLatinStream("#"));
    }

    @Test
    public void sentenceSreamTest() {
        assertEquals("ordkay 1gasay ids3iay 4mm3tay llaay", PigLatin.pigLatinStream("kord s1ga iids3 t4mm3 all"));
    }

    @Test
    public void emptySentenceSreamTest() {
        assertEquals("", PigLatin.pigLatinStream(""));
    }

    @Test
    public void sentenceSreamPiecesTest() {
        assertEquals("ordkay 1gasay ids3iay 4mm3tay llaay", PigLatin.pigLatinStreamPieces("kord s1ga iids3 t4mm3 all"));
    }


}