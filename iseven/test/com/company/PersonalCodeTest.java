package com.company;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.company.PersonalCode.Gender.FEMALE;
import static com.company.PersonalCode.Gender.MALE;
import static org.junit.Assert.assertEquals;

public class PersonalCodeTest {
    private PersonalCode aikiPersonalCode = new PersonalCode("38003166510");
    private PersonalCode sanderPersonalCode = new PersonalCode("50709037093");
    private PersonalCode janePersonalCode = new PersonalCode("49403136515");

    @Test
    public void birthdayTest() {
        assertEquals("1980-03-16", aikiPersonalCode.getDateOfBirth().toString());
        assertEquals("2007-09-03", sanderPersonalCode.getDateOfBirth().toString());
    }

    @Test
    public void getCenturyTest() {
        assertEquals(1900, aikiPersonalCode.getCentury(3));
        assertEquals(2000, sanderPersonalCode.getCentury(5));
    }

    @Test
    public void getAgeOnTest() {
        LocalDate date = LocalDate.of(2019, 3, 18);
        assertEquals(39, aikiPersonalCode.getAgeOn(date));
        assertEquals(11, sanderPersonalCode.getAgeOn(date));
    }

    @Test
    public void getGenderTest() {
        assertEquals(MALE, aikiPersonalCode.getGender());
        assertEquals(FEMALE, janePersonalCode.getGender());
    }

    @Test
    public void sortTest() {
        List<PersonalCode> v = new ArrayList<>();
        v.add(new PersonalCode("38003166510"));
        v.add(new PersonalCode("49603136526"));
        v.add(new PersonalCode("50709037093"));
        v.add(new PersonalCode("49403136515"));
        v.sort(new PersonalCodeSortAgainstGenderAndBirthDay());

        List<PersonalCode> expected = new ArrayList<>(Arrays.asList(
                new PersonalCode("49403136515"),
                new PersonalCode("49603136526"),
                new PersonalCode("38003166510"),
                new PersonalCode("50709037093")));
        assertEquals(expected, v);
    }

    @Test
    public void sortWithNullTest() {
        List<PersonalCode> v = new ArrayList<>();
        v.add(new PersonalCode("38003166510"));
        v.add(new PersonalCode(null));
        v.add(null);
        v.add(new PersonalCode("49603136526"));
        v.add(new PersonalCode("50709037093"));
        v.add(new PersonalCode("49403136515"));
        v.sort(new PersonalCodeSortAgainstGenderAndBirthDay());

        List<PersonalCode> expected = new ArrayList<>(Arrays.asList(
                new PersonalCode("49403136515"),
                new PersonalCode("49603136526"),
                new PersonalCode("38003166510"),
                new PersonalCode("50709037093"),
                new PersonalCode(null),
                null));
        assertEquals(expected, v);
    }

    @Test
    public void sortWithMergeSortTest() {
        List<PersonalCode> v = new ArrayList<>();
        v.add(new PersonalCode("38003166510"));
        v.add(new PersonalCode(null));
        v.add(null);
        v.add(new PersonalCode("49603136526"));
        v.add(new PersonalCode("69603136515"));
        v.add(new PersonalCode("50709037093"));
        v.add(new PersonalCode("49403136515"));
        PersonalCode.sort(v, new PersonalCodeSortAgainstGenderAndBirthDay());

        List<PersonalCode> expected = new ArrayList<>(Arrays.asList(
                new PersonalCode("49403136515"),
                new PersonalCode("49603136526"),
                new PersonalCode("69603136515"),
                new PersonalCode("38003166510"),
                new PersonalCode("50709037093"),
                new PersonalCode(null),
                null));
        assertEquals(expected, v);
    }

}