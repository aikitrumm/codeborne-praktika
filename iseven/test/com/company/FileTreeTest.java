package com.company;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FileTreeTest {

    @Test
    public void appendFileTest() {
        String expected = "    ConcatTest.class\n";
        String pathname = "./out/test/iseven/com/company/ConcatTest.class";
        StringBuilder beautifiedTree = new StringBuilder();
        assertEquals(expected, FileTree.appendDirectoryOrFile(new File(pathname), beautifiedTree, 1).toString());
    }

    @Test
    public void fileTreeTest() {
        String expected = "" +
                "    test\n" +
                "        com\n" +
                "            company\n" +
                "                ConcatTest.java\n" +
                "                PalindromeTest.java\n" +
                "                MultiplicationTableTest.java\n" +
                "                GuessingGameTest.java\n" +
                "                ReverseListTest.java\n" +
                "                FileTreeTest.java\n" +
                "                StreamFilterTest.java\n" +
                "                FibonacciTest.java\n" +
                "                SortTest.java\n" +
                "                StreamTest.java\n" +
                "                MainTest.java\n" +
                "                PigLatinTest.java\n";
        String pathname = "./test";
        StringBuilder beautifiedTree = new StringBuilder();
        assertEquals(expected, FileTree.fileTree(new File(pathname), beautifiedTree, 1));
    }
}