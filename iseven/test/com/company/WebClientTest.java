package com.company;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class WebClientTest {
    @Test
    public void givenClientWhenServerRespondsWhenStartedThenCorrect() throws IOException {
        WebClient client = new WebClient();
        client.startConnection("127.0.0.1", 8999);
        String response = client.sendMessage("hello server");
        assertEquals("hello client", response);
    }

}