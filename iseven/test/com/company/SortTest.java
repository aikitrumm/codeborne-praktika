package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortTest {

    @Test
    public void sortEmptyArrayTest() {
        int[] array = new int[] {};
        int[] sortedArray = new int[] {};
        assertArrayEquals(sortedArray, Sort.sortWithSort(array));
    }

    @Test
    public void sortOneIntegerArrayTest() {
        int[] array = new int[] {1};
        int[] sortedArray = new int[] {1};
        assertArrayEquals(sortedArray, Sort.sortWithSort(array));
    }

    @Test
    public void sortTwoIntegerArrayTest() {
        int[] array = new int[] {2, 1};
        int[] sortedArray = new int[] {1, 2};
        assertArrayEquals(sortedArray, Sort.sortWithSort(array));
    }

    @Test
    public void sortEmptyArrayBubbleSortAlgorithmTest() {
        int[] array = new int[] {};
        int[] sortedArray = new int[] {};
        assertArrayEquals(sortedArray, Sort.sortWithBubbleSort(array));
    }

    @Test
    public void sortOneIntegerArrayBubbleSortAlgorithmTest() {
        int[] array = new int[] {2};
        int[] sortedArray = new int[] {2};
        assertArrayEquals(sortedArray, Sort.sortWithBubbleSort(array));
    }

    @Test
    public void sortThreeIntegerArrayBubbleSortAlgorithmTest() {
        int[] array = new int[] {4, 2, 1};
        int[] sortedArray = new int[] {1, 2, 4};
        assertArrayEquals(sortedArray, Sort.sortWithBubbleSort(array));
    }

    @Test
    public void sortManyIntegersArrayBubbleSortAlgorithmTest() {
        int[] array = new int[] {5, 3, 2, 1, 4};
        int[] sortedArray = new int[] {1, 2, 3, 4, 5};
        assertArrayEquals(sortedArray, Sort.sortWithBubbleSort(array));
    }

    @Test
    public void sortManyIntegersBackwardsArrayBubbleSortAlgorithmTest() {
        int[] array = new int[] {9, 7, 3, 2, 1};
        int[] sortedArray = new int[] {1, 2, 3, 7, 9};
        assertArrayEquals(sortedArray, Sort.sortWithBubbleSort(array));
    }

    @Test
    public void sortManyIntegersNoSortNeededArrayBubbleSortAlgorithmTest() {
        int[] array = new int[] {1, 2, 3, 4, 5};
        int[] sortedArray = new int[] {1, 2, 3, 4, 5};
        assertArrayEquals(sortedArray, Sort.sortWithBubbleSort(array));
    }

}