package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EqualIsNotTest {

    EqualIsNot equalIsNot = new EqualIsNot();

    @Test
    public void equalIsNot() {
        assertTrue(equalIsNot.equalIsNot("isnot"));
    }

    @Test
    public void countOfIs() {
        assertEquals(1, equalIsNot.countOfIs("is"));
        assertEquals(2, equalIsNot.countOfIs("isis"));
        assertEquals(2, equalIsNot.countOfIs("areisareis"));
    }

    @Test
    public void countOfNot() {
        assertEquals(1, equalIsNot.countOfNot("not"));
        assertEquals(2, equalIsNot.countOfNot("notnot"));
        assertEquals(2, equalIsNot.countOfNot("arenotarenot"));
    }
}