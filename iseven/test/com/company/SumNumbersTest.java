package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumNumbersTest {

    SumNumbers sumNumbers = new SumNumbers();

    @Test
    public void sumNumbers() {
        assertEquals(0, sumNumbers.sumNumbers(""));
        assertEquals(1, sumNumbers.sumNumbers("1"));
        assertEquals(1, sumNumbers.sumNumbers("a1a"));
        assertEquals(11, sumNumbers.sumNumbers("a11a"));
        assertEquals(7, sumNumbers.sumNumbers("a3fgd4f"));
        assertEquals(44, sumNumbers.sumNumbers("aa11b33"));
    }
}