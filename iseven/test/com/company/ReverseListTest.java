package com.company;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.company.ReverseList.reverseNumbersRecursive;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

class ReverseListTest {

    @Test
    void reverseListOneMemberTest() {
        List<Integer> numbers = new ArrayList<>(Collections.singletonList(2));
        List<Integer> reversedNumbers = new ArrayList<>(Collections.singletonList(2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbers(numbers));
    }

    @Test
    void reverseListTwoMembersTest() {
        List<Integer> numbers = new ArrayList<>(asList(2, 7));
        List<Integer> reversedNumbers = new ArrayList<>(asList(7, 2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbers(numbers));
    }

    @Test
    void reverseListThreeMembersTest() {
        List<Integer> numbers = new ArrayList<>(asList(2, 7, 11));
        List<Integer> reversedNumbers = new ArrayList<>(asList(11, 7, 2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbers(numbers));
    }

    @Test
    void reverseListWhileLoopOneMemberTest() {
        List<Integer> numbers = new ArrayList<>(Collections.singletonList(2));
        List<Integer> reversedNumbers = new ArrayList<>(Collections.singletonList(2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbersWhileLoop(numbers));
    }

    @Test
    void reverseListWhileLoopTwoMembersTest() {
        List<Integer> numbers = new ArrayList<>(asList(2, 5));
        List<Integer> reversedNumbers = new ArrayList<>(asList(5, 2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbersWhileLoop(numbers));
    }

    @Test
    void reverseListWhileLoopThreeMembersTest() {
        List<Integer> numbers = new ArrayList<>(asList(2, 5, -11));
        List<Integer> reversedNumbers = new ArrayList<>(asList(-11, 5, 2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbersWhileLoop(numbers));
    }

    @Test
    void reverseListRecursiveOneMemberTest() {
        List<Integer> numbers = new ArrayList<>(Collections.singletonList(2));
        List<Integer> reversedNumbers = new ArrayList<>(Collections.singletonList(2));
        assertEquals(reversedNumbers, ReverseList.reverseNumbersRecursive(numbers));
    }

    @Test
    void reverseListRecursiveFourMembersTest() {
        List<Integer> numbers = asList(1, 2, 3, 4);
        List<Integer> reversedNumbers = asList(4, 3, 2, 1);
        assertEquals(reversedNumbers, reverseNumbersRecursive(numbers));
    }

}