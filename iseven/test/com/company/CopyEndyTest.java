package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class CopyEndyTest {

    CopyEndy copyEndy = new CopyEndy();

    @Test
    public void copyEndy() {
        assertArrayEquals(new int[]{9}, copyEndy.copyEndy(new int[]{9}, 1));
        assertArrayEquals(new int[]{9}, copyEndy.copyEndy(new int[]{9, 20}, 1));
        assertArrayEquals(new int[]{9}, copyEndy.copyEndy(new int[]{9, 90}, 1));
    }

    @Test
    public void isEndy() {
        assertTrue(copyEndy.isEndy(0));
        assertTrue(copyEndy.isEndy(100));
        assertFalse(copyEndy.isEndy(11));
        assertFalse(copyEndy.isEndy(50));
    }
}