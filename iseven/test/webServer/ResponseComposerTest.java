package webServer;

import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class ResponseComposerTest {

    private ResponseComposer responseComposer = new ResponseComposer();

    @Test
    public void compose() {
        ResponseMessage responseMessage = responseComposer.compose(StatusCode.OK, "text/plain", "hello nimi".getBytes(StandardCharsets.UTF_8), RequestMethod.GET);

        assertEquals("HTTP/1.1 200 OK\n", responseMessage.responseLine);
        assertEquals("Content-Type: text/plain; charset=utf-8;\nContent-Length: 10\n\n", responseMessage.responseHeader);
        assertEquals(10, responseMessage.messageBody.length);
    }

    @Test
    public void composeNotFound() {
        ResponseMessage responseMessage = responseComposer.compose(StatusCode.NOTFOUND, "text/html", "".getBytes(StandardCharsets.UTF_8), RequestMethod.GET);

        assertEquals("HTTP/1.1 404 Not Found\n", responseMessage.responseLine);
        assertEquals("Content-Type: text/html; charset=utf-8;\nContent-Length: 0\n\n", responseMessage.responseHeader);
        assertEquals(0, responseMessage.messageBody.length);
    }
}