package webServer;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResponseByteHelloAndGoodbyeComposerTest {
    private ResponseHelloAndGoodbyeComposer responseHelloAndGoodbyeComposer = new ResponseHelloAndGoodbyeComposer();

    @Test
    public void composeHello() {
        ResponseHelloAndGoodbye responseHelloAndGoodbye = responseHelloAndGoodbyeComposer.compose("hello", "");
        assertEquals("Hello", responseHelloAndGoodbye.messageBody);
        assertEquals(5, responseHelloAndGoodbye.length);
    }

    @Test
    public void composeGoodbye() {
        ResponseHelloAndGoodbye responseHelloAndGoodbye = responseHelloAndGoodbyeComposer.compose("goodbye", "");
        assertEquals("Goodbye", responseHelloAndGoodbye.messageBody);
        assertEquals(7, responseHelloAndGoodbye.length);
    }

    @Test
    public void composeHelloWorld() {
        ResponseHelloAndGoodbye responseHelloAndGoodbye = responseHelloAndGoodbyeComposer.compose("", "");
        assertEquals("Hello World!", responseHelloAndGoodbye.messageBody);
        assertEquals(12, responseHelloAndGoodbye.length);
    }

    @Test
    public void composeHellNo() {
        ResponseHelloAndGoodbye responseHelloAndGoodbye = responseHelloAndGoodbyeComposer.compose("Hell", "no");
        assertEquals("Hell no", responseHelloAndGoodbye.messageBody);
        assertEquals(7, responseHelloAndGoodbye.length);

    }
    @Test
    public void composeSpecialCharacters() {
        ResponseHelloAndGoodbye responseHelloAndGoodbye = responseHelloAndGoodbyeComposer.compose("Jää", "äär");
        assertEquals("Jää äär", responseHelloAndGoodbye.messageBody);
        assertEquals(11, responseHelloAndGoodbye.length);
    }

}