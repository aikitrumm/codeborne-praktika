package webServer;

import org.junit.Test;

import java.net.URISyntaxException;
import java.util.*;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class URIdentifierParserTest {

    private URIdentifierParser urIdentifierParser = new URIdentifierParser();

    @Test
    public void parser() throws URISyntaxException {
        URIdentifier urIdentifier = urIdentifierParser.parse("/file.txt?name=nimi&id=01&naMe=Smith");

        assertEquals("/file.txt", urIdentifier.path);
        assertEquals("name", urIdentifier.querys.get(0).name);
        assertEquals("nimi", urIdentifier.querys.get(0).value);
        assertEquals("id", urIdentifier.querys.get(1).name);
        assertEquals("01", urIdentifier.querys.get(1).value);
        assertEquals("naMe", urIdentifier.querys.get(2).name);
        assertEquals("Smith", urIdentifier.querys.get(2).value);
        assertEquals(3, urIdentifier.querys.size());
    }

    @Test
    public void parserSpecialCharacters() throws URISyntaxException {
        URIdentifier urIdentifier = urIdentifierParser.parse("/Jõudu?name=külamees");

        assertEquals("/Jõudu", urIdentifier.path);
        assertEquals("name", urIdentifier.querys.get(0).name);
        assertEquals("külamees", urIdentifier.querys.get(0).value);
        assertEquals(1, urIdentifier.querys.size());
    }

    @Test
    public void parserEmptyQuery() throws URISyntaxException {
        URIdentifier urIdentifier = urIdentifierParser.parse("/Jõudu");

        assertEquals("/Jõudu", urIdentifier.path);
        assertEquals(Collections.emptyList(), urIdentifier.querys);
    }
    @Test
    public void parserEmptyNameAndValue() throws URISyntaxException {
        URIdentifier urIdentifier = urIdentifierParser.parse("/Jõudu?name=&=külamees");

        assertEquals("/Jõudu", urIdentifier.path);
        assertEquals("name", urIdentifier.querys.get(0).name);
        assertEquals("", urIdentifier.querys.get(0).value);
        assertEquals("", urIdentifier.querys.get(1).name);
        assertEquals("külamees", urIdentifier.querys.get(1).value);
        assertEquals(2, urIdentifier.querys.size());
    }
}