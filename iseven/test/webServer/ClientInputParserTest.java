package webServer;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class ClientInputParserTest {
    private ClientInputParser clientInputParser = new ClientInputParser();

    @Test
    public void parse() {
        String inputStreamText = "" +
                "GET /helloworld.jpg HTTP/1.1\n" +
                "Host: localhost:8889\n" +
                "User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0)\n" +
                "Content-Length: 10\n\n";

        InputStream targetStream = new ByteArrayInputStream(inputStreamText.getBytes(StandardCharsets.UTF_8));
        BufferedReader in = new BufferedReader(new InputStreamReader(targetStream));

        ClientInput clientInput = clientInputParser.parse(in);

        assertEquals("GET /helloworld.jpg HTTP/1.1", clientInput.inputLine);
        assertEquals("Host", clientInput.headers.get(0).name);
        assertEquals("localhost:8889", clientInput.headers.get(0).value);
        assertEquals("User-Agent", clientInput.headers.get(1).name);
        assertEquals("10", clientInput.headers.get(2).value);
        assertEquals(3, clientInput.headers.size());
    }

    @Test
    public void parseWithoutRequestHeaders() {
        String inputStreamText = "" +
                "HEAD /helloworld.jpg HTTP/1.1\n" +
                "\n\n";

        InputStream targetStream = new ByteArrayInputStream(inputStreamText.getBytes(StandardCharsets.UTF_8));
        BufferedReader in = new BufferedReader(new InputStreamReader(targetStream));

        ClientInput clientInput = clientInputParser.parse(in);

        assertEquals("HEAD /helloworld.jpg HTTP/1.1", clientInput.inputLine);
        assertEquals(0, clientInput.headers.size());
    }
}