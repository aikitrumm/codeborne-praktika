package webServer;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class RequestParserTest {

    private RequestParser requestParser = new RequestParser();

    @Test
    public void parseFile() throws URISyntaxException, IOException {
        char[] requestBody = new char[]{};
        List<Header> headers = new ArrayList<>();
        ClientInput clientInput = new ClientInput("GET /file.txt HTTP/1.1", headers, requestBody);

        Request request = requestParser.parse(clientInput);
        assertEquals("/file.txt", request.uri.path);
        assertTrue(request.isFileAsked);

    }

    @Test
    public void parseHello() throws URISyntaxException, IOException {
        char[] requestBody = new char[]{};
        List<Header> headers = new ArrayList<>();
        ClientInput clientInput = new ClientInput("GET /Hell?name=no HTTP/1.1", headers, requestBody);

        Request request = requestParser.parse(clientInput);
        assertEquals("/Hell", request.uri.path);
        assertEquals("/Hell?name=no", request.line.path);
    }
}