package webServer;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class RequestedFileParserTest {

    private RequestedFileParser requestedFileParser = new RequestedFileParser();

    @Test
    public void parse() throws IOException {
        RequestedFile requestedFileTxt = requestedFileParser.parse("/file.txt", RequestMethod.GET);
        RequestedFile requestedFileJpg = requestedFileParser.parse("/helloworld.jpg", RequestMethod.GET);
        RequestedFile requestedFileNotFound = requestedFileParser.parse("/filenotfound.ext", RequestMethod.GET);

        assertEquals("file.txt", requestedFileTxt.filename);
        assertEquals("txt", requestedFileTxt.fileExtension);
        assertEquals(StatusCode.OK, requestedFileTxt.statusCode);
        assertEquals("text/plain", requestedFileTxt.mimeType);
        assertTrue(requestedFileTxt.isFile);

        assertEquals("image/jpeg", requestedFileJpg.mimeType);
        assertTrue(requestedFileJpg.isFile);

        assertEquals("filenotfound.ext", requestedFileNotFound.filename);
        assertEquals("text/html", requestedFileNotFound.mimeType);
        assertEquals(StatusCode.NOTFOUND, requestedFileNotFound.statusCode);
        assertFalse(requestedFileNotFound.isFile);
    }
}