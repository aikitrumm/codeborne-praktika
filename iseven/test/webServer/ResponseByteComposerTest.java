package webServer;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;

public class ResponseByteComposerTest {

    private ResponseTypeComposer responseTypeComposer = new ResponseTypeComposer();

    @Test
    public void compose() throws IOException {
        String headers = "headers";
        byte[] fileBytes = {1, 2, 3};
        byte[] expectedOutput = {104, 101, 97, 100, 101, 114, 115, 1, 2, 3};

        Response responseType = responseTypeComposer.compose(RequestMethod.GET, new ResponseMessage("", headers, fileBytes));
        assertArrayEquals(expectedOutput, responseType.bytes);
    }
}