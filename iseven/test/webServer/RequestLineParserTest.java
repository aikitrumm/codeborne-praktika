package webServer;

import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.assertEquals;

public class RequestLineParserTest {

    private RequestLineParser requestLineParser = new RequestLineParser();

    @Test
    public void parse() throws UnsupportedEncodingException {
        RequestLine lineGet = requestLineParser.parse("GET /foo?name=Smith HTTP/1.1");
        RequestLine lineHead = requestLineParser.parse("HEAD /foo.txt HTTP/1.0");
        RequestLine linePost = requestLineParser.parse("POST / HTTP/1.1");

        assertEquals(RequestMethod.GET, lineGet.method);
        assertEquals("HTTP/1.1", lineGet.httpVersion);
        assertEquals("/foo?name=Smith", lineGet.path);


        assertEquals(RequestMethod.HEAD, lineHead.method);
        assertEquals("HTTP/1.0", lineHead.httpVersion);
        assertEquals("/foo.txt", lineHead.path);

        assertEquals(RequestMethod.POST, linePost.method);
        assertEquals("HTTP/1.1", linePost.httpVersion);
        assertEquals("/", linePost.path);

    }
}