package webServer;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ResponseLogicTest {

    private ResponseLogic responseLogic = new ResponseLogic();

    @Test
    public void composeIsFile() throws IOException {
        ResponseData responseData = responseLogic.compose(true, "file.txt", "", RequestMethod.GET);
        assertEquals("text/plain", responseData.mimeType);
        assertEquals(StatusCode.OK, responseData.statusCode);
    }

    @Test
    public void composeIndexHtml() throws IOException {
        ResponseData responseData = responseLogic.compose(true, "", "", RequestMethod.GET);
        assertEquals("text/html", responseData.mimeType);
        assertEquals(StatusCode.OK, responseData.statusCode);
    }

    @Test
    public void composeNoFileFound() throws IOException {
        ResponseData responseData = responseLogic.compose(true, "fille.txt", "", RequestMethod.GET);
        assertEquals("text/html", responseData.mimeType);
        assertEquals(StatusCode.NOTFOUND, responseData.statusCode);
    }

    @Test
    public void composeFileNotAsked() throws IOException {
        ResponseData responseData = responseLogic.compose(false, "nimi", "", RequestMethod.GET);
        assertEquals("text/plain", responseData.mimeType);
        assertEquals(StatusCode.OK, responseData.statusCode);
    }
}