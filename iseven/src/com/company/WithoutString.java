package com.company;

public class WithoutString {

    String withoutString(String base, String remove) {

        return base.replaceAll("(?i)"+remove, "");
    }
}
