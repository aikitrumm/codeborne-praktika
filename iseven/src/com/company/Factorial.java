package com.company;

public class Factorial {

    public static void main(String[] args) {
        System.out.println(factorialTailRecursion(5, 1));
    }

    static int factorial(int number) {
        if (number <= 1) {
            return 1;
        }
        return number * factorial(number-1);
    }

    static int factorialTailRecursion(int number, int result) {
        if (number <= 1) {
            return result;
        }
        return factorialTailRecursion(number - 1, number*result);
    }
}
