package com.company;

public class HasOne {
    public boolean hasOne(int n) {

        if (n==10) return true;

        while (n > 99 && n%10 != 1) {
            n = n/10;
        }

        return n%10 == 1;
    }
}
