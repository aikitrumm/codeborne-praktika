package com.company;

import java.time.*;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

class PersonalCode implements Comparable<PersonalCode> {

    String personalCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalCode that = (PersonalCode) o;
        return Objects.equals(personalCode, that.personalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personalCode);
    }

    PersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    LocalDate getDateOfBirth() {
        int firstNumber = getNumber(0, 1);
        int year = getNumber(1, 3);
        Month month = Month.of(getNumber(3, 5));
        int day = getNumber(5, 7);
        return LocalDate.of(getCentury(firstNumber) + year, month, day);
    }

    private int getNumber(int startIndex, int endIndex) {
        return Integer.parseInt(this.personalCode.substring(startIndex, endIndex));
    }

    int getAgeOn(LocalDate date) {
        return Period.between(getDateOfBirth(), date).getYears();
    }

    long getDaysOn(LocalDate date) {
        return DAYS.between(date, this.getDateOfBirth());
    }

    int getCentury(int firstNumber) {
        if (firstNumber == 1 || firstNumber == 2) {
            return 1800;
        } else if (firstNumber == 3 || firstNumber == 4) {
            return 1900;
        } else if (firstNumber == 5 || firstNumber == 6) {
            return 2000;
        } else if (firstNumber == 7 || firstNumber == 8) {
            return 2100;
        }
        return 0;
    }

    // https://et.wikipedia.org/wiki/Isikukood
    Gender getGender() {
        int firstNumber = getNumber(0, 1);
        Gender gender = null;
        Set<Integer> male = new HashSet<>(Arrays.asList(1, 3, 5, 7));
        Set<Integer> female = new HashSet<>(Arrays.asList(2, 4, 6, 8));

        if (male.contains(firstNumber)) {
            gender = Gender.MALE;
        } else if (female.contains(firstNumber)) {
            gender = Gender.FEMALE;
        }
        return gender;
    }

    @Override
    public String toString() {
        return "PersonalCode{" + personalCode + "}";
    }

    // ASCII (F)EMALE 70  (M)ALE 77
    @Override
    public int compareTo(PersonalCode personalCode) {
        String gender = String.valueOf(personalCode.getGender());

        //noinspection UseCompareMethod
        if (String.valueOf(this.getGender()).compareTo(gender) == 0) {
            return 0;
        } else if (String.valueOf(this.getGender()).compareTo(gender) > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    enum Gender {
        MALE, FEMALE
    }

    static <T> void sort(List<T> list, Comparator<T> comparator) {

        mergeSort(list, list.size(), comparator);

    }

    private static <T> void mergeSort(List<T> list, int listSize, Comparator<T> comparator) {
        if (listSize < 2) {
            return;
        }
        int middle = listSize / 2;
        List<T> leftList = new ArrayList<>(list.subList(0, middle));
        List<T> rightList = new ArrayList<>(list.subList(middle, listSize));

        mergeSort(leftList, middle, comparator);
        mergeSort(rightList, listSize - middle, comparator);

        merge(list, leftList, rightList, middle, listSize - middle, comparator);
    }

    private static <T> void merge(List<T> list, List<T> leftList, List<T> rightList, int left, int right, Comparator<T> comparator) {
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < left && j < right) {
            if (comparator.compare(leftList.get(i), rightList.get(j)) <= 0) {
                list.set(k++, leftList.get(i++));
            } else {
                list.set(k++, rightList.get(j++));
            }
        }
        while (i < left) {
            list.set(k++, leftList.get(i++));
        }
        while (j < right) {
            list.set(k++, rightList.get(j++));
        }
    }

    public static void main(String[] args) {
        List<PersonalCode> v = new ArrayList<>();
        v.add(new PersonalCode(null));
        v.add(new PersonalCode("50709037093"));
        v.add(new PersonalCode("49603136515"));
        v.add(new PersonalCode("69603136515"));
        v.add(new PersonalCode("38003166510"));
        v.add(new PersonalCode("49403136515"));
        sort(v, new PersonalCodeSortAgainstGenderAndBirthDay());
        System.out.println(v);
    }
}
