package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

public class Fibonacci {

    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>(asList(0));
        List<Integer> b = new ArrayList<>(asList(0, 1));
        System.out.println(b);
        int b1 = b.get(b.size() - 2);
        int b2 = b.get(b.size() - 1);
        int sum = b.get(b.size() - 2) + b.get(b.size() - 1);
        // b.add(sum);
        System.out.println(b1 + ", " + b2 + ", " + sum);
        System.out.println(b);
        fibonacciTailRecursionList(5, a, b);
    }

    static List<Integer> fibonacciRecursive(int numbersInSequence) {
        if (numbersInSequence == 1) {
            return Collections.singletonList(0);
        } else if (numbersInSequence == 2) {
            return new ArrayList<>(asList(0, 1));
        } else {
            List<Integer> fibonacciNumbers = new ArrayList<>(fibonacciRecursive(numbersInSequence - 1));
            fibonacciNumbers.add(fibonacciNumbers.get(fibonacciNumbers.size() - 1) + fibonacciNumbers.get(fibonacciNumbers.size() - 2));
            return fibonacciNumbers;
        }

    }

    static List<Integer> fibonacciForLoop(int numbersInSequence) {
        if (numbersInSequence == 1) {
            return Collections.singletonList(0);
        } else if (numbersInSequence == 2) {
            return Arrays.asList(0, 1);
        } else {
            List<Integer> fibonacciNumbers = new ArrayList<>(Arrays.asList(0, 1));
            for (int i = 2; i < numbersInSequence; i++) {
                fibonacciNumbers.add(fibonacciNumbers.get(fibonacciNumbers.size() - 1) + fibonacciNumbers.get(fibonacciNumbers.size() - 2));
            }
            return fibonacciNumbers;
        }
    }

    static List<Integer> fibonacciWhileLoop(int numbersInSequence) {
        if (numbersInSequence == 1) {
            return Collections.singletonList(0);
        } else if (numbersInSequence == 2) {
            return Arrays.asList(0, 1);
        } else {
            List<Integer> fibonacciNumbers = new ArrayList<>(Arrays.asList(0, 1));
            int i = 0;
            while (i < numbersInSequence - 2) {
                fibonacciNumbers.add(fibonacciNumbers.get(fibonacciNumbers.size() - 1) + fibonacciNumbers.get(fibonacciNumbers.size() - 2));
                i++;
            }
            return fibonacciNumbers;
        }
    }

    private static int fibonacciTailRecursion(int numbersInSequence, int a, int b) {
        if (numbersInSequence == 0) {
            return a;
        } else if (numbersInSequence == 1) {
            return b;
        } else {
            return fibonacciTailRecursion(numbersInSequence - 1, b, a + b);
        }
    }

    static List<Integer> fibonacciTailRecursionList(int numbersInSequence, List<Integer> a, List<Integer> b) {
        if (numbersInSequence == 1) {
            return a;
        } else if (numbersInSequence == 2) {
            return b;
        } else {
            int sum = b.get(b.size() - 2) + b.get(b.size() - 1);
            a.add(sum);
            b.add(sum);
            return fibonacciTailRecursionList(numbersInSequence - 1, a, b);
        }
    }

}
