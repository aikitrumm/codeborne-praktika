package com.company;

import java.util.ArrayList;
import java.util.List;

public class MultiplicationTable {

    public static void main(String[] args) {
        System.out.println(multiplicationTableRendering(10, 10));
    }

    static String multiplicationTableFixedCellWidth(int width, int height) {
        int cellWidth = String.valueOf(width * height).length() + 1;
        StringBuilder firstLine = new StringBuilder(String.format("%" + cellWidth + "s |", "*"));

        for (int l = 1; l <= width; l++) {
            firstLine.append(String.format("%" + cellWidth + "s", l));
        }

        String lineStart = "";
        String nextInLine = "";
        StringBuilder wholeLine = new StringBuilder();

        for (int j = 1; j <= height; j++) {
            StringBuilder numbersInNextInLine = new StringBuilder();
            for (int i = 1; i <= width; i++) {
                numbersInNextInLine.append(String.format("%" + cellWidth + "s", i * j));
            }
            lineStart = String.format("%" + cellWidth + "s |", j);
            nextInLine = numbersInNextInLine + "\n";
            wholeLine.append(lineStart).append(nextInLine);
        }
        return firstLine + "\n" + wholeLine;
    }

    static String multiplicationTableRendering(int width, int height) {

        StringBuilder firstTwoLines = firstTwoLines(width, height);
        StringBuilder wholeTable = createMultiplicationTable(width, height);

        return firstTwoLines.append(wholeTable).toString();
    }

    static StringBuilder createMultiplicationTable(int width, int height) {
        int cellWidthFirstColumn = String.valueOf(height).length() + 1;
        List<List<Integer>> multiplicationTable = multiplicationTable(width, height);

        StringBuilder wholeTable = new StringBuilder();
        for (int i = 0; i <= multiplicationTable.size() - 1; i++) {
            List<Integer> line = multiplicationTable.get(i);
            StringBuilder numbersInLine = createMultiplicationTableNextInLine(width, height, line);
            wholeTable.append(String.format("%" + cellWidthFirstColumn + "s" + " |", i + 1)).append(numbersInLine).append("\n");
        }
        return wholeTable;
    }

    static StringBuilder createMultiplicationTableNextInLine(int width, int height, List<Integer> line) {
        List<Integer> cellWidth = multiplicationTableCellWidth(width, height);

        StringBuilder numbersInLine = new StringBuilder();
        for (int j = 0; j <= line.size() - 1; j++) {
            numbersInLine.append(String.format("%" + cellWidth.get(j) + "s", String.valueOf(line.get(j))));
        }
        return  numbersInLine;
    }

    static StringBuilder firstTwoLines(int width, int height) {
        List<Integer> cellWidth = multiplicationTableCellWidth(width, height);

        int widthAsterixCell = String.valueOf(height).length() + 1;
        String firstLineStart = String.format("%" + widthAsterixCell + "s" + " |", "*");

        StringBuilder nextInFirstLine = new StringBuilder();
        for (int i = 1; i <= cellWidth.size(); i++) {
            nextInFirstLine.append(String.format("%" + cellWidth.get(i - 1) + "s", i));
        }

        StringBuilder wholeFirstLine = new StringBuilder();
        wholeFirstLine.append(firstLineStart).append(nextInFirstLine);

        int lineLength = wholeFirstLine.length();
        StringBuilder secondLine = new StringBuilder();
        for (int i = 0; i < lineLength; i++) {
            secondLine.append("-");
        }

        return wholeFirstLine.append("\n").append(secondLine).append("\n");
    }

    static List<Integer> multiplicationTableCellWidth(int width, int height) {
        List<List<Integer>> multiplicationTable = multiplicationTable(width, height);
        List<Integer> listOfCellWidths = new ArrayList<>();

        for (int j = 0; j <= width - 1; j++) {
            int cellWidth = String.valueOf(multiplicationTable.get(height - 1).get(j)).length() + 1;
            listOfCellWidths.add(cellWidth);
        }

        return listOfCellWidths;
    }

    static List<List<Integer>> multiplicationTable(int width, int height) {
        List<List<Integer>> twoDArray = new ArrayList<>();

        for (int i = 1; i <= height; i++) {
            List<Integer> line = new ArrayList<>();
            for (int j = 1; j <= width; j++) {
                line.add(i * j);
            }
            twoDArray.add(line);
        }

        return twoDArray;
    }
}
