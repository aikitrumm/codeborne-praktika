package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TempDelete {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(bubbleSort(new int[]{5, 4, 3, 2, 1})));
        System.out.println(Arrays.toString(bubbleSortOptimized(new int[]{5, 4, 3, 2, 1})));
    }

    private static int[] bubbleSort(int[] array) {
        int temp;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j-1]>array[j]) {
                    temp = array[j-1];
                    array[j-1] = array[j];
                    array[j] = temp;
                }
            }

        }
        return array;
    }

    private static int[] bubbleSortOptimized(int[] array) {
        int temp;
        boolean swapped;
        for (int i = 0; i < array.length - 1; i++) {
            swapped = false;
            for (int j = 1; j < array.length - i; j++) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                    swapped = true;
                }

            }
            if (!swapped) break;
        }
        return array;
    }
}
