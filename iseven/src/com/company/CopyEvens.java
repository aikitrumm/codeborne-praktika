package com.company;

public class CopyEvens {
    public int[] copyEvens(int[] nums, int count) {
        int[] evenNumbers = new int[count];
        int j = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0) {
                evenNumbers[j] = nums[i];
                j++;
                count--;
            }
            if (count == 0) break;
        }
        return evenNumbers;
    }
}
