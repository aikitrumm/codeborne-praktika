package com.company;

import java.time.LocalDate;
import java.util.Comparator;

public class PersonalCodeSortAgainstGenderAndBirthDay implements Comparator<PersonalCode> {


    @Override
    public int compare(PersonalCode personalCode1, PersonalCode personalCode2) {
        LocalDate date = LocalDate.now();

        if (personalCode1 == null && personalCode2 == null) {
            return 0;
        } else if (personalCode1 == null) {
            return 1;
        } else if (personalCode2 == null) {
            return -1;
        } else if (personalCode1.personalCode == null && personalCode2.personalCode == null) {
            return 0;
        } else if (personalCode1.personalCode == null) {
            return 1;
        } else if (personalCode2.personalCode == null) {
            return -1;
        } else {
            if (personalCode1.getGender() == personalCode2.getGender()) {
                return (int) (personalCode1.getDaysOn(date) - personalCode2.getDaysOn(date));
            } else {
                return (personalCode2.getGender().compareTo(personalCode1.getGender()));
            }
        }
    }
}
