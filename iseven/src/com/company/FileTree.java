package com.company;

import java.io.File;
import java.util.Objects;

public class FileTree {

    public static void main(String[] args) {
        StringBuilder beautifiedFileTree = new StringBuilder();
        int directoryDepth = 1;
        File file = new File("./test");
        System.out.println(fileTree(file, beautifiedFileTree, directoryDepth));
    }

    static String fileTree(File file, StringBuilder beautifiedFileTree, int directoryDepth) {
        if (!file.isDirectory()) {
            throw new IllegalArgumentException("Not a directory");
        }

        appendDirectoryOrFile(file, beautifiedFileTree, directoryDepth);

        for (File member : Objects.requireNonNull(file.listFiles())) {
            if (member.isDirectory()) {
                fileTree(member, beautifiedFileTree, directoryDepth + 1);
            } else {
                appendDirectoryOrFile(member, beautifiedFileTree, directoryDepth + 1);
            }
        }
        return beautifiedFileTree.toString();
    }

    static StringBuilder appendDirectoryOrFile(File file, StringBuilder beautifiedFileTree, int directoryDepth) {
        return beautifiedFileTree
                .append(String.format("%" + directoryDepth * 4 + "s", " "))
                .append(file.getName())
                .append("\n");
    }
}
