package com.company;

public class CommonTwo {
    public int commonTwo(String[] a, String[] b) {
        int count = 0;
        int k = 0;

        for (int i = 0; i < a.length; i++) {
            if (i > 0 && a[i].equals(a[i - 1])) continue;
            for (int j = 0; j < b.length; j++) {
                if (j > 0 && b[j].equals(b[j - 1])) continue;
                if (a[i].compareTo(b[j]) == 0) {
                    count++;
                }
            }
        }
        return count;
    }
}
