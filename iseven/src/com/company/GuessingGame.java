package com.company;

import java.util.Scanner;

public class GuessingGame {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int randomNumber = (int) ((Math.random() * 100) + 1);
        System.out.println(randomNumber);

        ui(reader, randomNumber);

    }

    static void ui(Scanner reader, int randomNumber) {

        System.out.print("Arva number vahemikus 1-100\n[q] lõpetab): ");

        while (true) {
            String number = reader.nextLine();

            if (isQ(number)) {

                break;

            } else if (notQOrNumber(number)) {

                System.out.println("Sisesta number või lõpeta [q]: ");

            } else if (isNumberSmaller(randomNumber, number)) {

                System.out.println("Saad uuesti arvata. Number peab olema suurem.");
                System.out.print("Paku uus number: ");

            } else if (isNumberBigger(randomNumber, number)) {

                System.out.println("Saad uuesti arvata. Number peab olema väiksem.");
                System.out.print("Paku uus number: ");

            } else if (isNumberEqual(randomNumber, number)){

                System.out.println("Pakkusid õigesti ja arvasid numbri ära!");
                break;
            }
        }
    }

    static boolean notQOrNumber(String number) {
        return !isQ(number) && !isANumber(number);
    }

    static boolean isQ(String number) {
        return number.equals("q");
    }

    static boolean isNumberEqual(int randomNumber, String number) {
        return isANumber(number) && Integer.parseInt(number) == randomNumber;
    }

    static boolean isNumberBigger(int randomNumber, String number) {
        return isANumber(number) && Integer.parseInt(number) > randomNumber;
    }

    static boolean isNumberSmaller(int randomNumber, String number) {
        return isANumber(number) && Integer.parseInt(number) < randomNumber;
    }

    static boolean isANumber(String strNumber) {
        try {
            Integer.parseInt(strNumber);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
