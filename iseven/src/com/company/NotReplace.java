package com.company;

public class NotReplace {
    public String notReplace(String str) {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            boolean isAfterWordIsALetter = i < str.length() - 2 && str.substring(i, i + 2).equals("is") && Character.isLetter(str.charAt(i + 2));
            boolean isBeforeWordIsALetter = i > 0 && i < str.length() - 1 && str.substring(i, i + 2).equals("is") && Character.isLetter(str.charAt(i - 1));
            boolean isWordIs = i < str.length() - 1 && str.substring(i, i + 2).equals("is");

            if (isAfterWordIsALetter) {
                result.append(str.charAt(i));
                continue;
            }
            if (isBeforeWordIsALetter) {
                result.append(str.charAt(i));
                continue;
            }
            if (isWordIs) {
                result.append("is not");
                i++;
            } else {
                result.append(str.charAt(i));
            }
        }

        return result.toString();
    }
}
