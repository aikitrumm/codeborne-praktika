package com.company;

import java.util.HashMap;
import java.util.Map;

public class WordAppend {

    public String wordAppend(String[] strings) {
        Map<String, Integer> stringMap = new HashMap<>();
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < strings.length; i++) {
            if (stringMap.containsKey(strings[i])) {
                stringMap.put(strings[i], stringMap.get(strings[i]) + 1);
            } else {
                stringMap.put(strings[i], 1);
            }
            if (stringMap.get(strings[i])%2 == 0) {
                result.append(strings[i]);
            }
        }
        return result.toString();
    }
}
