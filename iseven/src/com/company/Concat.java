package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

class Concat {

    List<String> concat(List<String> stringList1, List<String> stringList2) {
        stringList1.addAll(stringList2);
        return stringList1;
    }

    List<String> concatFor(List<String> stringList1, List<String> stringList2) {
        List<String> stringListResult = new ArrayList<>(stringList1);
        for (String member : stringList2) {
            //noinspection UseBulkOperation
            stringListResult.add(member);
        }
        return stringListResult;
    }

}
