package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class MergeSort<T extends Comparable<? super T>> {

    static <T> List<T> sort(List<T> list, Comparator<T> comparator) {

        mergeSort(list, list.size(), comparator);
        return list;

    }

    private static void mergeSort(List list, int listSize, Comparator comparator){
        if (listSize < 2) {
            return;
        }
        int middle = listSize / 2;
        List leftList = new ArrayList();
        List rightList = new ArrayList();

        for (int i = 0; i < middle; i++) {
            leftList.add(i, list.get(i));
        }
        for (int i = middle; i < listSize; i++) {
            rightList.add(i - middle, list.get(i));
        }

        mergeSort(leftList, middle, comparator);
        mergeSort(rightList, listSize - middle, comparator);

        merge(list, leftList, rightList, middle, listSize - middle, comparator);
    }

    private static void merge(List list, List leftList, List rightList, int left, int right, Comparator comparator){
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < left && j < right) {
            if (comparator.compare(leftList.get(i), rightList.get(j)) <= 0) {
                list.add(k++, leftList.get(i++));
            } else {
                list.add(k++, rightList.get(j++));
            }
        }
        while (i < left) {
            list.add(k++, leftList.get(i++));
        }
        while (j < right) {
            list.add(k++, rightList.get(j++));
        }
    }
}