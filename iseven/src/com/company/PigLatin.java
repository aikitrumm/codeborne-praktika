package com.company;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PigLatin {

    static String pigLatin(String sentence) {
        if (sentence.equals("")) {
            return "";
        }

        String[] sonadeList = sentence.split(" ");
        for (int i = 0; i < sonadeList.length; i++) {
            String word = sonadeList[i];

            String s = moveFirstLetterToEnd(word);
            String changedWord = appendAy(s);


            sonadeList[i] = changedWord;
        }

        return String.join(" ", sonadeList);
    }

    private static String moveFirstLetterToEnd(String word) {
        String firstLetter = word.substring(0, 1);
        return word.substring(1) + firstLetter;
    }

    private static String appendAy(String s) {
        return s + "ay";
    }


    static String pigLatinStream(String sentence) {
        if (sentence.equals("")) {
            return "";
        }

        return Arrays.stream(sentence.split(" "))



                .map(word -> word.substring(1).concat(word.substring(0, 1)))



                .map(word -> word.concat("ay"))



                .collect(Collectors.joining(" "));
    }

    static String pigLatinStreamPieces(String sentence) {
        Stream<String> wordList = Arrays.stream(sentence.split(" "))
                .map(word -> word.substring(1).concat((word.substring(0, 1))))
                .map(word -> word.concat("ay"));

        return wordList.collect(Collectors.joining(" "));
    }
}
