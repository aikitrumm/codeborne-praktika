package com.company;

public class GHappy {

    public boolean gHappy(String str) {

        boolean isHappy = false;
        if (str.length() == 0) isHappy = true;
        for (int i = 0; i < str.length(); i++) {
            boolean isCharacterG = str.charAt(i) == 'g';
            boolean isPreviousCharacterG = i > 0 && str.charAt(i - 1) == 'g';
            boolean isNextCharacterG = i < str.length() - 1 && str.charAt(i + 1) == 'g';

            if (isCharacterG) {
                isHappy = isPreviousOrNextCharacterG(isPreviousCharacterG, isNextCharacterG);
            }

        }

        return isHappy;
    }

    public boolean isPreviousOrNextCharacterG(boolean isPreviousCharacterG, boolean isNextCharacterG) {
        boolean isHappy;
        if (isPreviousCharacterG) {
            isHappy = true;
        } else {
            if (isNextCharacterG) {
                isHappy = true;
            } else {
                isHappy = false;
            }
        }
        return isHappy;
    }
}
