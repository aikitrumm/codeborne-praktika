package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here

        long startTimeIsEvenWithMod = System.currentTimeMillis();
        long counter3 = 0;
        for (long i = 0; i < 1000000000; i++) {
            if (isEvenWithMod(i)) {
                counter3++;
            }
        }
        long endTimeIsEvenWithMod = System.currentTimeMillis();

        System.out.println("isEvenWithMod: " + (endTimeIsEvenWithMod - startTimeIsEvenWithMod));

        long startTimeIsEven = System.currentTimeMillis();
        long counter = 0;
        for (long i = 0; i < 1000000000; i++) {
            if (isEven(i)) {
                counter++;
            }
        }
        long endTimeIsEven = System.currentTimeMillis();

        System.out.println("isEven: " + (endTimeIsEven - startTimeIsEven));
    }

    static boolean isEven (long i) {
        return i%2 == 0;
    }

    static boolean isEvenWithMod (long i) {
        return mod(i, 2) == 0;
    }

    static boolean isEvenDifferent(int i) {
        int resultDivide = i/2;
        int resultMultiple = resultDivide*2;
        return i == resultMultiple;
    }

    static boolean isEvenFlagVariable(int i) {
        boolean isEven = true;
        for (int j = 1; j <= i; j++) {
            isEven = !isEven;
        }
        return isEven;
    }

    static long mod (long dividend, long divisor) {
        long division = dividend/divisor;
        return dividend-(division*divisor);
    }

    static boolean isEvenString(int i) {
        boolean isEven = false;
        String number = "" + i;
        String lastNumber = number.substring(number.length() - 1);
        if (lastNumber.equals("0")
                || lastNumber.equals("2")
                || lastNumber.equals("4")
                || lastNumber.equals("6")
                || lastNumber.equals("8")) {
            isEven = true;
        }

        return isEven;
    }
}

