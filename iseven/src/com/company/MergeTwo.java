package com.company;

public class MergeTwo {
    public String[] mergeTwo(String[] a, String[] b, int n) {
        String[] result = new String[n];
        int compare = 0;
        int j = 0;
        int k = 0;

        for (int i = 0; i < n; i++) {
                compare = a[j].compareTo(b[k]);

                if (compare < 0) {
                    result[i] = a[j];
                    j++;
                } else if (compare == 0) {
                    result[i] = a[j];
                    j++;
                    k++;
                } else  {
                    result[i] = b[k];
                    k++;
                }
            }
        return result;
    }
}
