package com.company;

import java.util.ArrayList;
import java.util.List;

class ReverseList {

    static List<Integer> reverseNumbers(List<Integer> numbers) {
        List<Integer> reversedNumbers = new ArrayList<>();
        for (int i = numbers.size(); i > 0; i--) {
            reversedNumbers.add(numbers.get(i-1));
        }
        return reversedNumbers;
    }

    static List<Integer> reverseNumbersWhileLoop(List<Integer> numbers) {
        List<Integer> reversedNumbers = new ArrayList<>();
        int i = numbers.size()-1;
        while (i != -1) {
            reversedNumbers.add(numbers.get(i));
            i--;
        }
        return reversedNumbers;
    }

    static List<Integer> reverseNumbersRecursive(List<Integer> numbers) {
        if(numbers.size() > 1) {
            int value = numbers.remove(0);
            reverseNumbersRecursive(numbers);
            numbers.add(value);
        }
        return numbers;
    }
}
