package com.company;

import java.util.Arrays;

class Sort {
    static int[] sortWithSort(int[] array) {
        int[] sortedArray = Arrays.copyOf(array, array.length);
        Arrays.sort(sortedArray);
        return sortedArray;
    }

    static int[] sortWithBubbleSort(int[] array) {
        int[] sortedArray = Arrays.copyOf(array, array.length);
        for (int i = 0; i < sortedArray.length - 1; i++) {
            for (int j = 1; j < (sortedArray.length - i); j++) {
                if (sortedArray[j - 1] > sortedArray[j]) {
                    int temp = sortedArray[j - 1];
                    sortedArray[j - 1] = sortedArray[j];
                    sortedArray[j] = temp;
                }
            }
        }
        return sortedArray;
    }
}
