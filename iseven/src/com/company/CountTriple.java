package com.company;

public class CountTriple {
    public int countTriple(String str) {
        int count = 0;

        for (int i = 1; i < str.length()-1; i++) {
            if (str.length() >= 3 && str.charAt(i-1) == str.charAt(i) &&
                    str.charAt(i) == str.charAt(i+1)) {
                count++;
            }
        }
        return count;
    }
}
