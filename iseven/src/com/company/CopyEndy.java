package com.company;

public class CopyEndy {
    public int[] copyEndy(int[] nums, int count) {
        int[] endyNums = new int[count];
        int j = 0;

        for (int number : nums) {
            if (isEndy(number)) {
                endyNums[j] = number;
                j++;
                count--;
            }
            if (count == 0) break;
        }
        return endyNums;
    }

    boolean isEndy(int n) {
        return 0 <= n && n <= 10 || 90 <= n && n <= 100;
    }
}
