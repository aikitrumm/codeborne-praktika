package com.company;

public class EqualIsNot {
    final int IS_LENGTH = "is".length();
    final int NOT_LENGTH = "not".length();

    boolean equalIsNot(String str) {
        return countOfIs(str) == countOfNot(str);
    }

    int countOfIs(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - IS_LENGTH + 1; i++) {
            if (str.substring(i, i + IS_LENGTH).equals("is")) {
                count++;
                i = i + IS_LENGTH - 1;
            }
        }
        return count;
    }

    public int countOfNot(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - NOT_LENGTH + 1; i++) {
            if (str.substring(i, i + NOT_LENGTH).equals("not")) {
                count++;
                i = i + NOT_LENGTH - 1;
            }
        }
        return count;
    }
}
