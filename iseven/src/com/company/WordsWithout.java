package com.company;

public class WordsWithout {
    public String[] wordsWithout(String[] words, String target) {
        int count = 0;
        for (String word : words) {
            if (word.equals(target)) count++;
        }
        String[] result = new String[words.length - count];
        int j = 0;
        for (String word : words) {
            if (!word.equals(target)) {
                result[j] = word;
                j++;
            }
        }
        return result;
    }
}
