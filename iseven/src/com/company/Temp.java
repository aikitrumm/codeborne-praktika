package com.company;

import java.util.Arrays;

public class Temp {
    public static void main(String[] args) {

        System.out.println();
    }








    public static String plusOut(String str, String word) {
        String result = "";

        for (int i = 0; i <= str.length() - word.length(); i++) {
            if (!(str.substring(i, i + word.length()).equals(word)) ||
                str.length() - i - 1 == 0) {
                result += "+";
            }
            if (str.substring(i, i + word.length()).equals(word)) {
                result += word;
                i += word.length() - 1;
            }
        }

        return result;

    }









    public static int[] zeroMax(int[] nums) {
        int index = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                index = i;
                int biggestOdd = nums[i];
                for (int j = index; j < nums.length; j++) {
                    if (nums[j] == 0) {
                        nums[i] = biggestOdd;
                        biggestOdd = 0;
                    }
                    if (nums[j]%2 == 1 && nums[j] > biggestOdd) biggestOdd = nums[j];
                }
            }


        }
        return nums;
    }

    public static int[] zeroFront(int[] nums) {
        int count = 0;
        int[] grouped = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) count++;
        }
        int k = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != 0) {
                grouped[count+k] = nums[j];
                k++;
            }
        }
        return grouped;
    }

    public boolean twoTwo(int[] nums) {

        int streak = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i]==0) {
                streak++;
                if (streak > 2) {
                    return true;
                }
            } else {            // tails : streak is broken
                streak = 0;
            }
        }
        return false;
    }

    public static boolean sameEnds(int[] nums, int len) {

        int[] start = new int[len];
        for (int i = 0; i < len; i++) {
            start[i] = nums[i];
        }

        int[] end = new int[len];
        for (int j = 0; j < len; j++) {
            end[j] = nums[j + nums.length - len];
        }

        int count = 0;
        for (int k = 0; k < len; k++) {
            if (start[k] == end[k]) count++;
        }

        return count==len;

    }


    public int makeChocolate(int small, int big, int goal) {
        int smallNeeded = goal - big*5;
        int maxBigPossible = goal/5;

        if (smallNeeded >= 0 && smallNeeded <= small) {
            return smallNeeded;
        }

        if (goal - maxBigPossible*5 + small >= goal) {
            return (small - (goal - (goal - maxBigPossible*5 + small)));
        }

        return -1;
    }
    public boolean haveThree(int[] nums) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 3) {
                count++;
            }
        }

        for (int i = 1; i < nums.length - 1; i++) {
            if (nums[i - 1] == 3 && nums[i] != 3 ||
                    nums[i - 1] != 3 && nums[i] == 3 && nums[i+1] != 3) {
                return count == 3 && true;
            }
        }
        return false;
    }


    public static boolean has77(int[] nums) {
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] == 7 && nums[i+1] == 7 || nums[i] == 7 && nums[i+2] == 7) {
                return true;
            }
        }
        return false;
    }

    public boolean countCode(String str) {
        int count = 0;

        for (int i = 0; i < str.length() - 3; i++) {
            if (str.substring(i, i+2).equals("co") && str.substring(i+2, i+3).equals("e")) {
                count++;
            }
        }

        return count > 0;
    }
}


