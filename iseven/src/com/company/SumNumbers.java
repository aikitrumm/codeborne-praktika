package com.company;

public class SumNumbers {
    public int sumNumbers(String str) {

        int sum = 0;
        StringBuilder numberAsString = new StringBuilder();
        numberAsString.append(0);
        boolean isOneDigit = str.length() == 1 && Character.isDigit(str.charAt(0));
        if (isOneDigit) return Integer.parseInt(str.toString());

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                numberAsString.append(str.charAt(i));
            } else {
                sum += Integer.parseInt(numberAsString.toString());
                numberAsString.setLength(0);
                numberAsString.append(0);
            }
            if (i == str.length() - 1) {
                sum += Integer.parseInt(numberAsString.toString());
            }
        }
        return sum;
    }
}
