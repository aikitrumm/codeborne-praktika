package com.company;

class Palindrome {

    static boolean isPalindrome(String s) {
        StringBuilder reversed = new StringBuilder();
        String letter = "";
        for (int i = s.length(); i > 0; i--) {
            letter = s.substring(i-1, i);
            reversed.append(letter);
        }
        return s.equals(reversed.toString());
    }

    static boolean isPalindromeRecursive(String s) {
        if (s.length() <= 1) {
            return true;
        } else {
            return (s.charAt(0) == s.charAt(s.length()-1)) && isPalindromeRecursive(s.substring(1, s.length()-1));
        }

    }
}
