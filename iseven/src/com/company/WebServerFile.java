package com.company;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebServerFile {
    PrintWriter out;
    private static final HashMap<String, String> mimeTypes = new HashMap<>();

    static {
        mimeTypes.put("gif", "image/gif");
        mimeTypes.put("jpeg", "image/jpeg");
        mimeTypes.put("jpg", "image/jpeg");
        mimeTypes.put("jpe", "image/jpeg");
        mimeTypes.put("bmp", "image/bmp");
        mimeTypes.put("png", "image/png");
        mimeTypes.put("tif", "image/tiff");
        mimeTypes.put("tiff", "image/tiff");
        mimeTypes.put("jnlp", "application/x-java-jnlp-file");
        mimeTypes.put("js", "application/x-javascript");
        mimeTypes.put("doc", "application/msword");
        mimeTypes.put("bin", "application/octet-stream");
        mimeTypes.put("exe", "application/octet-stream");
        mimeTypes.put("pdf", "application/pdf");
        mimeTypes.put("ai", "application/postscript");
        mimeTypes.put("eps", "application/postscript");
        mimeTypes.put("ps", "application/postscript");
        mimeTypes.put("rtf", "application/rtf");
        mimeTypes.put("class", "application/x-java-vm");
        mimeTypes.put("ser", "application/x-java-serialized-object");
        mimeTypes.put("jar", "application/x-java-archive");
        mimeTypes.put("sh", "application/x-sh");
        mimeTypes.put("tar", "application/x-tar");
        mimeTypes.put("zip", "application/zip");
        mimeTypes.put("ua", "audio/basic");
        mimeTypes.put("wav", "audio/x-wav");
        mimeTypes.put("mid", "audio/x-midi");
        mimeTypes.put("htm", "text/html");
        mimeTypes.put("html", "text/html");
        mimeTypes.put("css", "text/css");
        mimeTypes.put("txt", "text/plain");
        mimeTypes.put("mpeg", "video/mpeg");
        mimeTypes.put("mpg", "video/mpeg");
        mimeTypes.put("mpe", "video/mpeg");
        mimeTypes.put("qt", "video/quicktime");
        mimeTypes.put("mov", "video/quicktime");
        mimeTypes.put("avi", "video/avi");
        mimeTypes.put("movie", "video/x-sgi-movie");
        mimeTypes.put("ico", "image/x-icon");
    }

    public static void main(String[] args) {
        WebServerFile server = new WebServerFile();
        server.start();
    }

    private void start() {
        int port = 8889;

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Listening for connection on port " + port + "\n");

            //noinspection InfiniteLoopStatement
            while (true) {
                Socket clientSocket = serverSocket.accept();
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8));

                List<String> clientInput = getClientInput(in);
                String[] requestArray = clientInput.get(0).split(" ");
                String requestType = requestArray[0];
                byte[] fileBytes;
                byte[] response;
                String url = getURL(clientInput);
                URI uri = new URI(url);
                String path = uri.getPath().substring(1);
                String query = uri.getQuery();
                String name = ifExistsGetNameFromQuery(query);

                if (requestType.equals("GET")) {
                    if (path.contains(".") && isFile(path)) {
                        fileBytes = readAndReturnFile(path);
                        String fileExtension = getFileExtension(path);
                        String headers = makeHeaders(fileBytes, "200 OK", fileExtension);
                        response = makeResponse(headers, fileBytes);
                    } else if (path.contains(".")) {
                        byte[] error404Bytes = readAndReturn404File();
                        String headers = makeHeaders(error404Bytes, "404 Not Found", "html");
                        response = makeResponse(headers, error404Bytes);
                    } else {
                        byte[] responseBodyBytes = getBodyForHelloAndGoodbye(path, name);
                        String headers = makeHeaders(responseBodyBytes, "200 OK", "txt");
                        response = makeResponse(headers, responseBodyBytes);
                    }
                } else {
                    if (path.contains(".") && isFile(path)) {
                        fileBytes = readAndReturnFile(path);
                        String fileExtension = getFileExtension(path);
                        String headers = makeHeaders(fileBytes, "200 OK", fileExtension);
                        response = makeResponse(headers, new byte[0]);
                    } else if (path.contains(".")) {
                        byte[] error404Bytes = readAndReturn404File();
                        String headers = makeHeaders(error404Bytes, "404 Not Found", "html");
                        response = makeResponse(headers, new byte[0]);
                    } else {
                        byte[] responseBodyBytes = getBodyForHelloAndGoodbye(path, name);
                        String headers = makeHeaders(responseBodyBytes, "200 OK", "");
                        response = makeResponse(headers, new byte[0]);
                    }

                }
                sendResponseToClient(clientSocket.getOutputStream(), response);

                stop(in, clientSocket);
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private String getURL(List<String> clientInput) {
        String[] getArray = clientInput.get(0).split(" ");
        String[] hostArray = clientInput.get(1).split(" ");
        return "http://" + hostArray[1] + getArray[1];

    }

    String ifExistsGetNameFromQuery(String query) {
        if (query != null && query.contains("name")) {
            String[] queryString = query.split("&");
            Map<String, String> queryList = new HashMap<>();
            for (String member : queryString) {
                queryList.put(member.split("=")[0], member.split("=")[1]);
            }
            return queryList.get("name");
        } else {
            return "";
        }
    }

    private boolean isFile(String path) {
        File file = new File(path);
        return file.exists() && !file.isDirectory();
    }

    private byte[] readAndReturnFile(String path) {
        File file = new File(path);
        byte[] bytes = null;
        try {
            bytes = Files.readAllBytes(Paths.get(file.getPath()));
        } catch (IOException e) {
            return bytes;
        }
        return bytes;
    }

    String makeHeaders(byte[] fileBytes, String statusCode, String fileExtension) {
        String fullStatusCode = "HTTP/1.0 " + statusCode + "\n";
        String contentType = "Content-Type: " + mimeTypes.get(fileExtension) + "; charset=utf-8\n";
        String contentLength = "Content-Length: " + fileBytes.length + "\n\n";
        return fullStatusCode + contentType + contentLength;
    }

    byte[] getBodyForHelloAndGoodbye(String path, String name) throws UnsupportedEncodingException {
        if (path.equals("hello")) {
            return "Hello".getBytes();
        } else if (path.equals("goodbye")) {
            return "Goodbye".getBytes();
        } else if (path.length()>0 && name.length() == 0) {
            return ("Hello " + URLDecoder.decode(path, "UTF-8")).getBytes();
        } else if (name.length() > 0 && path.length() > 0){
            return (URLDecoder.decode(path, "UTF-8") + " " + URLDecoder.decode(name, "UTF-8")).getBytes();
        } else {
            return "Hello World!".getBytes();
        }

    }

    byte[] makeResponse(String headers, byte[] fileBytes) {
        byte[] headersAsBytes = headers.getBytes(StandardCharsets.UTF_8);
        byte[] fullResponse = new byte[headersAsBytes.length + fileBytes.length];
        System.arraycopy(headersAsBytes, 0, fullResponse, 0, headersAsBytes.length);
        System.arraycopy(fileBytes, 0, fullResponse, headersAsBytes.length, fileBytes.length);
        return fullResponse;
    }

    void sendResponseToClient(OutputStream outputStream, byte[] fullResponse) {
        try {
            outputStream.write(fullResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] readAndReturn404File() {
        File file = new File("404.html");
        byte[] bytes = null;
        try {
            bytes = Files.readAllBytes(Paths.get(file.getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    String getFileExtension(String path) {
        String[] fileExtension = path.split("\\.");
        if (fileExtension.length > 1) {
            return fileExtension[1];
        } else {
            return fileExtension[0];
        }
    }

    List<String> getClientInput(BufferedReader in) {
        List<String> inputLineList = new ArrayList<>();
        String inputLine;
        while (true) {
            try {
                if ((inputLine = in.readLine()) != null) {
                    System.out.println(inputLine);
                    if (inputLine.length() > 0) {
                        inputLineList.add(inputLine);
                    }
                    if (inputLine.length() == 0) {
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return inputLineList;
    }

    private void stop(BufferedReader in, Socket clientSocket) throws IOException {
            in.close();
            clientSocket.close();
    }
}
