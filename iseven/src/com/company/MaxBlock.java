package com.company;

public class MaxBlock {
    public int maxBlock(String str) {

        int count = 0;
        int biggestCount = 0;
        if (str.length() >= 1) {
            biggestCount++;
            count++;
        }

        for (int i = 0; i < str.length(); i++) {
            boolean isCurrentCharacterSameAsNext = i < str.length() - 1 && str.charAt(i) == str.charAt(i + 1);
            if (isCurrentCharacterSameAsNext) {
                count++;
                if (count > biggestCount) {
                    biggestCount = count;
                }
            } else {
                count = 1;
            }
        }
        return biggestCount;
    }
}
