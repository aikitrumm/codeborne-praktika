package com.company;

public class DividesSelf {

    public boolean dividesSelf(int n) {
        int modifiableN = n;
        if (modifiableN < 10) return true;

        while (modifiableN > 0) {
            int divider = modifiableN % 10;
            if (divider == 0) return false;
            if (n%divider != 0) return false;
            modifiableN = modifiableN / 10;
        }

        return true;
    }
}
