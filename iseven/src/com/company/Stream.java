package com.company;

import java.util.Arrays;

class Stream {

    static int[] doubleInput(int[] array) {

        return Arrays.stream(array)
                .map(member -> member * 2)
                .toArray();
    }

    static int[] evenAndDoubleInput(int[] array) {

        return Arrays.stream(array)
                .filter(member -> member % 2 == 0)
                .map(member -> member * 2)
                .toArray();
    }

    static int[] returnPrimeAndDoubleAndSorted(int[] array) {
        return Arrays.stream(array)
                .filter(member -> {
                    if (member < 2) return false;
                    for (int i = 2; i < member; i++) {
                        if (member % i == 0) {
                            return false;
                        }
                    }
                    return true;
                })
                .map(member -> member * 2)
                .sorted()
                .toArray();
    }

    static int[] doubleInputForLoop(int[] array) {
        int[] doubleArray = new int[array.length];
        for (int i = 0; i <= array.length - 1; i++) {
            doubleArray[i] = array[i] * 2;
        }
        return doubleArray;
    }

}
