package com.company;

import java.util.ArrayList;
import java.util.List;

class StreamFilter {

    interface Filter {
        boolean function(int number);
    }

    class EvenMembers implements Filter {
        @Override
        public boolean function(int number) {
            return number%2 == 0;
        }
    }

    class OddMembers implements Filter {
        @Override
        public boolean function(int number) {
            return number%2 != 0;
        }
    }

    List<Integer> filterEvenNumbersInterface(List<Integer> numbers) {
        return filter(numbers, number -> number%2 == 0);
    }

    List<Integer> filterOddNumbersInterface(List<Integer> numbers) {
        return filter(numbers, number -> number%2 != 0);
    }

    private static List<Integer> filter(List<Integer> numbers, Filter filter) {
        List<Integer> result = new ArrayList<>();
        for (Integer member : numbers) {
            if (filter.function(member)) {
                result.add(member);
            }
        }
        return result;
    }



    static List<Integer> filterEvenNumbers(List<Integer> numbers) {
        List<Integer> evenNumbers = new ArrayList<>();

        for (Integer member : numbers) {
            if (member%2 == 0) {
                evenNumbers.add(member);
            }
        }
        return evenNumbers;
    }

    static List<Integer> filterOddNumbers(List<Integer> numbers) {
        List<Integer> oddNumbers = new ArrayList<>();

        for (Integer member : numbers) {
            if (member % 2 != 0) {
                oddNumbers.add(member);
            }
        }
        return oddNumbers;
    }



}
