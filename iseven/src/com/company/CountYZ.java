package com.company;


public class CountYZ {

    int countYZ(String str) {
        int count = 0;
        count += countOfYOrZAsLastCharactersInWord(str);
        if (isLastCharacterInStringYOrZ(str)) {
            count++;
        }
        return count;
    }

    boolean isLastCharacterInStringYOrZ(String str) {
        boolean isLastCharacterInStringY = Character.toLowerCase(str.charAt(str.length() - 1)) == 'y';
        boolean isLastCharacterInStringZ = Character.toLowerCase(str.charAt(str.length() - 1)) == 'z';
        return isLastCharacterInStringY || isLastCharacterInStringZ;
    }

    int countOfYOrZAsLastCharactersInWord(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            boolean isLastCharacterAndNextIsNonCharacter = Character.isLetter(str.charAt(i)) && !Character.isLetter(str.charAt(i + 1));
            if (isLastCharacterAndNextIsNonCharacter) {
                boolean isCharacterXorY = Character.toLowerCase(str.charAt(i)) == 'y' || Character.toLowerCase(str.charAt(i)) == 'z';
                if (isCharacterXorY) {
                    count++;
                }
            }
        }
        return count;
    }
}
