package webServer;

class RequestedFile {
    boolean isFile;
    byte[] file;
    String filename;
    String fileExtension;
    String mimeType;
    StatusCode statusCode;

    RequestedFile(boolean isFile, String filename, String fileExtension, byte[] fileBytes, StatusCode statusCode, String mimeType) {
        this.isFile = isFile;
        this.filename = filename;
        this.fileExtension = fileExtension;
        this.file = fileBytes;
        this.mimeType = mimeType;
        this.statusCode = statusCode;
    }
}
