package webServer;

import java.io.File;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;

class RequestedFileParser {
    //todo
//    https://tools.ietf.org/html/rfc7231#section-9.1
//    https://portswigger.net/web-security/file-path-traversal

    RequestedFile parse(String path, RequestMethod method) throws IOException {

        String filename = getFilename(path);
        String fileExtension = getFileExtension(filename);
        StatusCode statusCode;

        boolean isFile = false;
        byte[] fileBytes;
        File file = new File(filename);
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType;
        if (filename.length() == 0 || "index.html".equalsIgnoreCase(filename)) {
            file = new File("index.html");
            fileBytes = Files.readAllBytes(Paths.get(file.getPath()));
            if (method == RequestMethod.POST) {
                statusCode = StatusCode.FOUND;
            } else {
                statusCode = StatusCode.OK;
            }
            mimeType = fileNameMap.getContentTypeFor(file.getName());
        } else if (file.exists() && !file.isDirectory() && !"index.html".equalsIgnoreCase(filename)) {
                statusCode = StatusCode.OK;
                fileBytes = Files.readAllBytes(Paths.get(file.getPath()));
                isFile = true;
                if ("ico".equals(fileExtension)) {
                    mimeType = "image/x-icon";
                } else {
                    mimeType = fileNameMap.getContentTypeFor(file.getName());
                }
        } else {
            file = new File("404.html");
            fileBytes = Files.readAllBytes(Paths.get(file.getPath()));
            statusCode = StatusCode.NOTFOUND;
            mimeType = fileNameMap.getContentTypeFor(file.getName());
        }

        return new RequestedFile(isFile, filename, fileExtension, fileBytes, statusCode, mimeType);
    }

    private String getFileExtension(String filename) {
        String fileExtension = "";
        String[] filenameArray = filename.split("\\.");
        if (filenameArray.length > 1) {
            fileExtension = filenameArray[filenameArray.length - 1];
        }
        return fileExtension;
    }

    String getFilename(String path) {
        String[] pathArray = path.split("/");
        String filename = "";
        if (pathArray.length > 0 && pathArray[pathArray.length - 1].contains(".")) {
            filename = pathArray[pathArray.length - 1];
        }
        return filename;
    }

    boolean deleteFile(String filename) {
        File file = new File(filename);
        return file.delete();
    }
}
