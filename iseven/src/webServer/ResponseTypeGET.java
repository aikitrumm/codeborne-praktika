package webServer;

import java.io.IOException;

class ResponseTypeGET extends Response {
    ResponseTypeGET(String responseLine, String responseHeader, byte[] messageBody) throws IOException {
        super(responseLine, responseHeader, messageBody);
    }
}
