package webServer;

import java.io.IOException;

class ResponseTypeComposer {

    Response compose(RequestMethod method, ResponseMessage responseMessage) throws IOException {

        if (method == RequestMethod.GET) {
            return new ResponseTypeGET (responseMessage.responseLine, responseMessage.responseHeader, responseMessage.messageBody);
        } else if (method == RequestMethod.HEAD) {
            return new ResponseTypeHEAD(responseMessage.responseLine, responseMessage.responseHeader);
        } else if (method == RequestMethod.DELETE) {
            return new ResponseTypeDELETE(responseMessage.responseLine, responseMessage.responseHeader, responseMessage.messageBody);
        } else {
            return new ResponseTypePOST(responseMessage.responseLine, responseMessage.responseHeader, responseMessage.messageBody);
        }


        //todo
//        When a request method is received
//        that is unrecognized or not implemented by an origin server, the
//        origin server SHOULD respond with the 501 (Not Implemented) status
//        code.
    }
}
