package webServer;

import java.util.ArrayList;
import java.util.List;

public class ResponseHeaderComposer {
    public ResponseHeader compose(String mimeType, int length, RequestMethod method) {
        List<Header> headers = new ArrayList<>();
        headers.add(new Header("Content-Type", mimeType + charset(mimeType)));
        headers.add(new Header("Content-Length", length + "\n"));
        if (method == RequestMethod.POST) {
            headers.add(new Header("Location", "/" + "\n"));
        }

        StringBuilder headersSB = new StringBuilder();
        for (Header header : headers) {
            headersSB.append(header.name).append(": ").append(header.value);
        }

        return new ResponseHeader(headersSB.toString());
    }

    private String charset(String mimeType) {
        String charset = "\n";
        if (mimeType.length() > 0 && "text".equals(mimeType.substring(0, 4))) {
            charset = "; charset=utf-8;\n";
        }
        return charset;
    }
}
