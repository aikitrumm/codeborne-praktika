package webServer;

import java.io.IOException;
import java.io.OutputStream;

class SendResponse {
    void send(OutputStream out, byte[] responseByte) throws IOException {
        out.write(responseByte);
    }
}
