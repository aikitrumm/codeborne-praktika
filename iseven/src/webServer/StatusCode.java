package webServer;

public enum StatusCode {
    OK(200, "OK"),
    NOTFOUND(404, "Not Found"),
    FOUND(302, "Found");

    public final int code;
    public final String status;

    StatusCode(int code, String status) {
        this.code = code;
        this.status = status;
    }
}
