package webServer;

import java.util.List;

class Request {
    RequestLine line;
    URIdentifier uri;
    List<Header> headers;
    boolean isFileAsked;
    char[] requestBody;

    Request(RequestLine line, URIdentifier uri, boolean isFileAsked, List<Header> headers, char[] requestBody) {
        this.line = line;
        this.uri = uri;
        this.headers = headers;
        this.isFileAsked = isFileAsked;
        this.requestBody = requestBody;
    }
}
