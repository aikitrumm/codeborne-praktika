package webServer;

class ResponseMessage {
    String responseLine;
    String responseHeader;
    byte[] messageBody;

    ResponseMessage(String responseLine, String responseHeader, byte[] messagebody) {
        this.responseLine = responseLine;
        this.responseHeader = responseHeader;
        this.messageBody = messagebody;
    }
}
