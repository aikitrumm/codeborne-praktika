package webServer;

import java.util.List;

class ClientInput {

    String inputLine;
    List<Header> headers;
    char[] requestBody;

    ClientInput(String inputLine, List<Header> headers, char[] requestBody) {
        this.inputLine = inputLine;
        this.headers = headers;
        this.requestBody = requestBody;
    }
}
