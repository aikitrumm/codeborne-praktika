package webServer;

class ResponseHelloAndGoodbye {
    String messageBody;
    int length;
    final StatusCode statusCode = StatusCode.OK;
    final String mimeType = "text/plain";

    ResponseHelloAndGoodbye(String messageBody, int length) {
        this.messageBody = messageBody;
        this.length = length;
    }
}
