package webServer;

import java.io.IOException;

class ResponseTypePOST extends Response {
    ResponseTypePOST(String responseLine, String responseHeader, byte[] messageBody) throws IOException {
        super(responseLine, responseHeader, messageBody);
    }
}
