package webServer;

class ResponseComposer {

    ResponseMessage compose(StatusCode statusCode, String mimeType, byte[] messagebody, RequestMethod method) {
        ResponseLine responseLine = new ResponseLine(statusCode);
        ResponseHeader responseHeader = new ResponseHeaderComposer().compose(mimeType, messagebody.length, method);

        return new ResponseMessage(responseLine.responseLine, responseHeader.responseHeader, messagebody);
    }
}
