package webServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

class ClientInputParser {

    ClientInput parse(BufferedReader in) {
        List<String> inputLineList = new ArrayList<>();
        String inputLine;
        char[] requestBody;
        int length = 0;
        List<Header> headers = new ArrayList<>();
        while (true) {
            try {
                if ((inputLine = in.readLine()) != null) {
                    System.out.println(inputLine);
                    if (inputLine.length() > 0) {
                        inputLineList.add(inputLine);
                    }
                    if (inputLine.length() == 0) {
                        for (int i = 1; i < inputLineList.size(); i++) {
                            String name = inputLineList.get(i).split(":", 2)[0].trim();
                            String value = inputLineList.get(i).split(":", 2)[1].trim();
                            Header header = new Header(name, value);
                            headers.add(header);
                        }

                        for (Header header : headers) {
                            if ("content-length".equalsIgnoreCase(header.name)) {
                                length = Integer.parseInt(header.value);
                                break;
                            }
                        }

                        requestBody = new char[length];
                        int read = 0, offset = 0;
                        while (offset < requestBody.length && read != -1) {
                            read = in.read(requestBody, offset, requestBody.length - offset); // https://stackoverflow.com/questions/38063548/intellij-result-of-inputstream-read-is-ignored-how-to-fix
                            offset += read;
                        }
                        System.out.println(URLDecoder.decode(String.valueOf(requestBody), "UTF-8"));
                        System.out.println(String.valueOf(requestBody));
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new ClientInput(inputLineList.get(0), headers, requestBody);
    }
}