package webServer;

import java.util.List;

class URIdentifier {
    String path;
    List<Query> querys;
    String name;

    URIdentifier(String path, List<Query> querys, String name) {
        this.path = path;
        this.querys = querys;
        this.name = name;
    }
}
