package webServer;

public class ResponseLine {
    String responseLine;

    ResponseLine(StatusCode statusCode) {
        this.responseLine = "HTTP/1.1 " + statusCode.code + " " + statusCode.status + "\n";
    }
}
