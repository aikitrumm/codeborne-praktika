package webServer;

import java.io.IOException;
import java.net.URISyntaxException;

class RequestParser {

    Request parse(ClientInput clientInput) throws URISyntaxException, IOException {

        RequestLine line = new RequestLineParser().parse(clientInput.inputLine);

        URIdentifier urIdentifier = new URIdentifierParser().parse(line.path);

        boolean isFileAsked = false;
        String[] pathArray = urIdentifier.path.split("/");
        if (pathArray.length == 0 || pathArray[pathArray.length - 1].contains(".")) {
            isFileAsked = true;
        }

        return new Request(line, urIdentifier, isFileAsked, clientInput.headers, clientInput.requestBody);
    }
}
