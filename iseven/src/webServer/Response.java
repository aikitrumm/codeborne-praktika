package webServer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

abstract class Response extends ResponseLogic{
    String responseLine;
    String responseHeader;
    byte[] responseBody;
    byte[] bytes;


    Response(String responseLine, String responseHeader, byte[] messageBody) throws IOException {
        this.responseLine = responseLine;
        this.responseHeader = responseHeader;
        this.responseBody = messageBody;
        this.bytes = getBytes(this.responseLine, this.responseHeader, this.responseBody);
    }

    final byte[] getBytes(String responseLine, String responseHeader, byte[] responseBody) {
        byte[] lineAsBytes = responseLine.getBytes(StandardCharsets.UTF_8);
        byte[] headersAsBytes = responseHeader.getBytes(StandardCharsets.UTF_8);
        byte[] response = new byte[lineAsBytes.length + headersAsBytes.length + responseBody.length];
        System.arraycopy(lineAsBytes, 0, response, 0, lineAsBytes.length);
        System.arraycopy(headersAsBytes, 0, response, lineAsBytes.length, headersAsBytes.length);
        System.arraycopy(responseBody, 0, response, lineAsBytes.length + headersAsBytes.length, responseBody.length);

        return response;
    }
}
