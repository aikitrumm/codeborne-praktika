package webServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args) {
        Main server = new Main();
        server.start();
    }

    private void start() {
        int port = 8999;

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Listening for connection on port " + port + "\n");

            //noinspection InfiniteLoopStatement
            while (true) {

                try (Socket clientSocket = serverSocket.accept();
                     BufferedInputStream input = new BufferedInputStream(clientSocket.getInputStream());
                     BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8));
                     OutputStream out = clientSocket.getOutputStream()) {


                    ClientInputParser clientInputParser = new ClientInputParser();
                    ClientInput clientInput = clientInputParser.parse(in );

                    RequestParser requestParser = new RequestParser();
                    Request request = requestParser.parse(clientInput);

                    ResponseData responseData = new ResponseLogic().compose(
                            request.isFileAsked,
                            request.uri.path.substring(1),
                            request.uri.name,
                            request.line.method);
                    ResponseMessage responseMessage = new ResponseComposer().compose(
                            responseData.statusCode,
                            responseData.mimeType,
                            responseData.messageBody,
                            request.line.method);

                    Response responseType = new ResponseTypeComposer().compose(request.line.method, responseMessage);

                    SendResponse sendResponse = new SendResponse();
                    sendResponse.send(out, responseType.bytes);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
