package webServer;

import java.io.IOException;

class ResponseTypeHEAD extends Response {
    ResponseTypeHEAD(String responseLine, String responseHeader) throws IOException {
        super(responseLine, responseHeader, new byte[]{});
    }
}
