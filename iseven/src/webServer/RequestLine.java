package webServer;

class RequestLine {
    RequestMethod method;
    String path;
    String httpVersion;

    RequestLine(RequestMethod method, String path, String httpVersion) {
        this.method = method;
        this.path = path;
        this.httpVersion = httpVersion;
    }
}
