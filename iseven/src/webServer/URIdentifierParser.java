package webServer;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

class URIdentifierParser {

    URIdentifier parse(String afterSlash) throws URISyntaxException {
        URI uri = new URI("http://fake.url" + afterSlash);
        String path = uri.getPath();
        String query = uri.getQuery();

        List<Query> querys = new ArrayList<>();
        if (query != null) {
            String[] queryArray = query.split("&");
            for (String member : queryArray) {
                String name = "";
                String value = "";
                String[] memberArray = member.split("=");
                if (memberArray[0].length() > 0) {
                    name = memberArray[0];
                }
                if (memberArray.length > 1) {
                    value = memberArray[1];
                }
                querys.add(new Query(name, value));
            }
        }

        StringBuilder allNamesInQuery = new StringBuilder();
        for (Query member : querys) {
            if ("name".equals(member.name)) {
                allNamesInQuery.append(member.value);
            }
        }
        return new URIdentifier(path, querys, allNamesInQuery.toString());
    }
}
