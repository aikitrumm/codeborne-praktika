package webServer;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

class RequestLineParser {

    RequestLine parse(String line) throws UnsupportedEncodingException {

        String[] requestLineArray = line.split(" ");

        RequestMethod requestMethod = RequestMethod.valueOf(requestLineArray[0]);
        String path = URLDecoder.decode(requestLineArray[1], "UTF-8");
        String httpVersion = requestLineArray[2].trim();

        return new RequestLine(requestMethod, path, httpVersion);
    }
}
