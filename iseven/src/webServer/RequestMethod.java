package webServer;

public enum RequestMethod {
    HEAD,
    GET,
    POST,
    DELETE;
}
