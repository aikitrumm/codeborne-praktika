package webServer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class ResponseLogic {
    ResponseData compose(boolean isFileAsked, String path, String name, RequestMethod method) throws IOException {

        byte[] messageBody;
        StatusCode statusCode;
        String mimeType;

        if (isFileAsked && method == RequestMethod.DELETE) {
            String filename = new RequestedFileParser().getFilename(path);
            boolean fileDeleted = new RequestedFileParser().deleteFile(filename);
            if (fileDeleted) {
                messageBody = new byte[]{};
                statusCode = StatusCode.OK;
                mimeType = "text/plain";
            } else {
                File file = new File("404.html");
                messageBody = Files.readAllBytes(Paths.get(file.getPath()));
                statusCode = StatusCode.NOTFOUND;
                mimeType = "text/html";
            }
        } else if (isFileAsked) {
            RequestedFile requestedFile = new RequestedFileParser().parse(path, method);
            messageBody = requestedFile.file;
            statusCode = requestedFile.statusCode;
            mimeType = requestedFile.mimeType;
        } else {
            ResponseHelloAndGoodbye responseHelloAndGoodbye = new ResponseHelloAndGoodbyeComposer().compose(path, name);
            messageBody = responseHelloAndGoodbye.messageBody.getBytes(StandardCharsets.UTF_8);
            statusCode = responseHelloAndGoodbye.statusCode;
            mimeType = responseHelloAndGoodbye.mimeType;
        }

        return new ResponseData(messageBody, statusCode, mimeType);
    }
}
