package webServer;

class ResponseData {

    byte[] messageBody;
    StatusCode statusCode;
    String mimeType;

    ResponseData(byte[] messageBody, StatusCode statusCode, String mimeType) {
        this.messageBody = messageBody;
        this.statusCode = statusCode;
        this.mimeType = mimeType;
    }
}