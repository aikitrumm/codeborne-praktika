package webServer;

import java.nio.charset.StandardCharsets;

class ResponseHelloAndGoodbyeComposer {

    ResponseHelloAndGoodbye compose(String path, String allNamesInQuery) {

        int length;
        String messageBody;
        boolean onlyPathExists = (path.length() > 0 && allNamesInQuery.length() == 0);
        boolean pathAndNameExists = (allNamesInQuery.length() > 0 && path.length() > 0);
        if ("hello".equals(path)) {
            messageBody = "Hello";
        } else if ("goodbye".equals(path)) {
            messageBody = "Goodbye";
        } else if(onlyPathExists) {
            messageBody = "Hello " + path;
        } else if(pathAndNameExists) {
            messageBody = path + " " + allNamesInQuery;
        } else {
            messageBody = "Hello World!";
        }

        length = messageBody.getBytes(StandardCharsets.UTF_8).length;
        return new ResponseHelloAndGoodbye(messageBody, length);
    }
}
